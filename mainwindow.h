#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListView>
#include <QLineEdit>
#include <QListWidget>
#include <QtOpcUa/QOpcUaClient>
#include <QtOpcUa/QOpcUaProvider>

#include <unistd.h>     /* for close() */
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <limits.h>
#include <arpa/inet.h> //inet_addr
#include <sys/socket.h>    //socket
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <arpa/inet.h> // for inet_ntoa()
#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip_icmp.h>   //Provides declarations for icmp header
#include <netinet/udp.h> //Provides declarations for udp header
#include <netinet/tcp.h> //Provides declarations for tcp header
#include <netinet/ip.h>  //Provides declarations for ip header
#include <arpa/inet.h> //inet_addr

#include <QMap>
#include <QString>
#include <QQueue>
#include <QMutex>

#include "utils.h"
#include "mtserver.h"
#include "sapex.h"
#include "utils.h"

#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned long
#define SOCKET int
#define SOCKET_ERROR -1
#define MIN(a,b) (a<b?a:b)
#define Sleep(a) usleep(a*1000*1000)
#define APP_NAME        "opcua client"
#define ONE_MILLISEC_USLEEP (1000)
#define ONE_SEC_USLEEP (1000*1000)
#define opcua_BUF_MAX_SIZE 1024

void mylog(unsigned char level,const char* fmt, ...);
void* opcuaDataListeningThread(void* ctx);

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    //bool init_sapex();
    //bool read_settings(QString& m_sSettingsFile)    ;
    //bool hasChildNodeItem(const QString &nodeId);
    //QSet<QString> mChildNodeIds;
    //QOpcUaNode* m_CDE_ORD_PAU;
    //QOpcUaClient *mOpcUaClient;
    //QOpcUaNode* rootNode;
    myOpcuaNode* current_node;
    //std::unique_ptr<QOpcUaNode> mOpcNode;
    //PAU_node* a_PAU_node;

    QListWidget* m_plist;
    QListWidget* m_plist2;
    QLineEdit* m_linedit;

    /*
    QString _local_ip_addr;
    QString _opcua_ip_addr;
    uint _opcua_port;
    QString _sapex_ip_addr;
    uint _sapex_port;
    bool                        _is_stopped;
    pthread_t                   _thread_id;
    bool                        _isConnected;
    int                         _sock;
    struct sockaddr_in          _sapex_server_sock;*/

private slots:
    /*void clientConnected();
    void clientDisconnected();
    void browseFinished(QVector<QOpcUaReferenceDescription> children, QOpcUa::UaStatusCode statusCode);
    void slot3(QOpcUa::NodeAttributes attr);
    void slot30(QOpcUa::NodeAttributes attr);
    void slot6(QOpcUa::NodeAttribute attr, const QVariant &value);*/
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_9_clicked();
    void on_pushButton_10_clicked();
    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_listWidget1_itemDoubleClicked(QListWidgetItem *item);

    void on_listWidget2_itemDoubleClicked(QListWidgetItem *item);

    void on_pushButton_13_clicked();

    void on_listWidget2_itemClicked(QListWidgetItem *item);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
