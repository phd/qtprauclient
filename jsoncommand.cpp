

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QMutex>

#include "globales.h"

void JSON_command::fromBuffer(unsigned char* buf,uint frameSize)
{
    if ( NULL!=buf && frameSize>0 ){
        memcpy(m_buf,buf,MIN(frameSize,JSON_MAX_SIZE));
        m_frameSize = frameSize;
        for (uint i=0 ; i< frameSize ; i++) {
            m_json_string += buf[i];
        }
    }
}

JSON_command::JSON_command(unsigned char* buf,uint frameSize)
{
    fromBuffer(buf,frameSize);
}

void JSON_command::extract_PAU_from_INIT()
{
    qDebug() << "JSON_command::getEventType:" << m_json_string;
    m_json_bytes = m_json_string.toLocal8Bit();
    auto json_doc = QJsonDocument::fromJson(m_json_bytes);
    if (json_doc.isNull()) {qDebug() << "Failed to create JSON doc." ;return ;}else qDebug() << "OK create JSON doc.";
    if (!json_doc.isObject()) {qDebug() << "JSON is not an object." ;return ;}qDebug() << "JSON is an object." ;
    QJsonObject json_obj = json_doc.object();
    if (json_obj.isEmpty()) {qDebug() << "JSON object is empty.";return ;}else qDebug() << "JSON object is not empty.";

    if(json_doc.isObject()) // objet racine ?
    {
        QJsonObject objetJSON = json_doc.object();
        QStringList listeCles;
        listeCles = objetJSON.keys();
        qDebug() << listeCles; // une liste de QString ex: trame INIT: ("data", "header", "type")

        if ( listeCles.contains("data") ){
            QJsonValue v1 = objetJSON.value("data");qDebug() << "data=" << v1;
            //"data":{"timestamp":"2020-12-21 08:23:55.233", "val":{"opc_server_name":"MTRAUServer", "opc_server_port":8383, "opc_sec_policy":"Basic256sha256", "opc_sec_mode":"Sign & encrypt", "pau_list":[{"PAU0000056":"PAU_01_0_00056"}, {"PAU0000053":"PAU_01_0_00053"}, {"PAU0000202":"PAU_02_1_00202"}, {"PAU0000000":"PAU_00_0_00000"}]}},
            QJsonObject objetJSON = v1.toObject();
            QStringList listeCles = objetJSON.keys(); qDebug() << "cles:" << listeCles; // ("timestamp", "val")
            if ( listeCles.contains("val") ){
                QJsonValue v2 = objetJSON.value("val");qDebug() << "val=" << v2;
                QJsonObject objetJSON = v2.toObject();
                QStringList listeCles = objetJSON.keys(); qDebug() << "cles:" << listeCles;
                //cles: ("opc_sec_mode", "opc_sec_policy", "opc_server_name", "opc_server_port", "pau_list")
                QJsonValue v3 = objetJSON.value("pau_list");qDebug() << "pau_list=" << v3;
                //pau_list= QJsonValue(array, QJsonArray([{"PAU0000056":"PAU_01_0_00056"},{"PAU0000053":"PAU_01_0_00053"},{"PAU0000202":"PAU_02_1_00202"},{"PAU0000000":"PAU_00_0_00000"}]))
                if(v3.isArray())
                {
                   qDebug() << "array=" << v3.toArray(); // le type QJsonArray
                   //array= QJsonArray([{"PAU0000056":"PAU_01_0_00056"},{"PAU0000053":"PAU_01_0_00053"},{"PAU0000202":"PAU_02_1_00202"},{"PAU0000000":"PAU_00_0_00000"}])
                   QJsonArray jsarr = v3.toArray();

                   //foreach (const QJsonValue & v, jsarr)qDebug() << v.toObject().value("ID").toInt();

                   for (uint i=0; i<jsarr.count() ; i++ ){
                       QJsonObject v4 = jsarr[i].toObject();
                       QStringList listeCles4 = v4.keys();
                       qDebug() << " element[" << i << "]=" << v4 << " keys:" << listeCles4 << " count=" << listeCles4.count() ;  // Pour obtenir la première valeur du tableau
                       for(int i = 0; i < listeCles4.count(); i++)
                       {
                           QJsonValue valeur = v4[listeCles4.at(i)];
                            qDebug() << " PAU=" << valeur.toString();
                            m_list_PAU.append(valeur.toString());
                       }
                   }
/*  element[ 0 ]= QJsonObject({"PAU0000056":"PAU_01_0_00056"})  keys: ("PAU0000056")
 element[ 1 ]= QJsonObject({"PAU0000053":"PAU_01_0_00053"})  keys: ("PAU0000053")
 element[ 2 ]= QJsonObject({"PAU0000202":"PAU_02_1_00202"})  keys: ("PAU0000202")
 element[ 3 ]= QJsonObject({"PAU0000000":"PAU_00_0_00000"})  keys: ("PAU0000000")*/
                }
            }
            return;

        }
    }
    return ;
}
