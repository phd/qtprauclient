#include "mainwindow.h"
#include "globales.h"

#include <QApplication>
#include <QFile>
#include <QSettings>


QAsyncQueue<JSON_command*> g_sapex_cmd;
QMap<QByteArray,QByteArray>   g_map1;
QMap<BYTE,QByteArray>     g_map2;

Bridge* theBridge;
SAPEX* theSapex;
myOpcuaServer* theOpcuaServer;

QMap<QString ,myOpcuaNode*>     g_object_node_children_map;
//QSet<QString> g_object_node_children_id;

QString _local_ip_addr;
QString _opcua_ip_addr;
uint _opcua_port;
QString _sapex_ip_addr;
uint _sapex_port;
QString MT_opcua_server_url(__MT_OPCUA_URL__);

bool read_settings(QString m_sSettingsFile);

void Bridge::run()
{
    mylog(2,"Bridge::run entered");
    while(1)
    {
        //mylog(2,"Bridge thread sleeping...");
        qDebug()<< "Bridge thread sleeping...";
        usleep(ONE_SEC_USLEEP);
        while ( !_eventsReceivedFromMT.isEmpty() ){
            mylog(2,"%d _commandsToSend",_eventsReceivedFromMT.count());
            MT_Event* ev = _eventsReceivedFromMT.pull();
            qDebug() << "MT_Event:"<<ev->m_eventType;
            if (ev->m_eventType == MT_Event::MT_CHGT_ETAT_PAU ){
                qDebug() << "MT_Event::MT_CHGT_ETAT_PAU:"<<ev->m_eventType;
                theSapex->sendJSONtoSAPEX(ev);
            }
        }
        while ( !_eventsReceivedFromSapex.isEmpty() ){
            mylog(2,"%d _commandsReceived",_eventsReceivedFromSapex.count());
            SAPEX_Event* pev = _eventsReceivedFromSapex.pull();//cstaLink->_commandsReceived.push(pCmd);
            if ( pev != NULL ) {
                //pCmd->hexdump();
                //processReceivedCommand(pCmd);
                qDebug() << "SAPEX_Event:"<<pev->m_eventType;
                if (pev->m_eventType == SAPEX_Event::PRESCOM_INIT ){
                    qDebug() << "PRESCOM_INIT:"<<pev->m_eventType;
                    qDebug() << "PRESCOM_INIT:"<<pev->m_list_PAU;
                    qDebug() << "PRESCOM_INIT:"<<pev->MT_opcua_server_url;
                    theOpcuaServer->m_list_PAU = pev->m_list_PAU;
                    theOpcuaServer->browseObjectNodeChildrenSubnodes();
                    delete pev;
                }
            }
        }
    }
    mylog(2,"CCSTALink::run exited");
    return;
}

int main(int argc, char *argv[])
{
    qDebug() << __DATE__ << __TIME__ ;
    qDebug() <<  "Compiled with Qt Version : " << QT_VERSION_STR;   //exit(0);
    qDebug() << "initialization done...";
    #define _STR(x) #x
    #define STRINGIFY(x)  _STR(x)
    printf("this was built on " STRINGIFY(BUILD_DATE) "\n");

    QApplication a(argc, argv);
    //myCoreApp a(argc, argv);

    QString m_sSettingsFile = QApplication::applicationDirPath() + "/params.ini";

    if ( read_settings( m_sSettingsFile) )    {
        theSapex = new SAPEX();
        theSapex->init(_sapex_ip_addr,_sapex_port);

        theOpcuaServer = new myOpcuaServer();
        theOpcuaServer->init(MT_opcua_server_url);        

        theBridge= new Bridge();
        theBridge->start();
    }
    MainWindow w;
    w.show();
    return a.exec();
}

bool read_settings(QString m_sSettingsFile)
{
    _local_ip_addr = "192.168.8.111";
    _opcua_ip_addr = "192.168.8.183";
    _opcua_port = 8383;
    //QUrl url("opc.tcp://192.168.8.183:8383");

    if (!QFile(m_sSettingsFile).exists()) return false;

    QSettings settings(m_sSettingsFile, QSettings::IniFormat);
    qDebug() << settings.fileName();

    _local_ip_addr = settings.value("local_address").toString();
    qDebug() << _local_ip_addr;

    _opcua_ip_addr = settings.value("opcua_server_address").toString();
    qDebug() << _opcua_ip_addr;
    //m_plist->addItem(QString("_opcua_ip_addr=%1").arg(_opcua_ip_addr) );

    _opcua_port = settings.value("opcua_server_port").toUInt();
    qDebug() << _opcua_port;
    //m_plist->addItem(QString("_opcua_port=%1").arg(_opcua_port));

    _sapex_ip_addr = settings.value("sapex_server_address").toString();
    qDebug() << _sapex_ip_addr;
    //m_plist->addItem(QString("_sapex_ip_addr=%1").arg(_sapex_ip_addr));

    _sapex_port = settings.value("sapex_server_port").toUInt();
    qDebug() << _sapex_port;
    //m_plist->addItem(QString("_sapex_port=%1").arg(_sapex_port));


    return true;
}
