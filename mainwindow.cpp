#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QMutex>
#include <QtCore>
#include <QString>
#include <QThread>
#include <QQueue>
#include <QMutex>
#include <QMap>
#include <QApplication>
#include <QCoreApplication>
#include <QTimer>
#include <QTextStream>
#include <QStringList>
#include <QLinkedList>
#include <QEvent>

#include "utils.h"
#include "mtserver.h"
#include "sapex.h"
#include "globales.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    current_node=NULL;
    //PAU_node* a_PAU_node;    a_PAU_node = NULL;
    //m_CDE_ORD_PAU = NULL;
    //mOpcUaProvider = NULL;
    //mOpcUaClient = NULL;
    //rootNode = NULL;
    ui->setupUi(this);
    //if ( m_linedit ){m_linedit = ui->lineEdit1; m_linedit->setText("33") ; }

    ui->lineEdit1->setText("77");
    m_plist = ui->listWidget1;
    m_plist2 = ui->listWidget2;
    if ( m_plist ){
        m_plist->addItem(QString("starting qt opcua program...")+__DATE__);
        /*QStringList items;
        items << "Argentine" << "Brazilian" << "South African"
              << "USA West" << "Monaco" << "Belgian" << "Spanish"
              << "Swedish" << "French" << "British" << "German"
              << "Austrian" << "Dutch" << "Italian" << "USA East"
              << "Canadian";
        m_plist->addItems( items );
        m_plist->insertItem(0,QString("insert item"));
        m_plist->insertItem(0,QString("insert item 2"));*/
    }


    //_local_ip_addr = "192.168.8.111";
    //_opcua_ip_addr = "192.168.8.183";
    //_opcua_port = 8383;

    /*
    QString m_sSettingsFile = QApplication::applicationDirPath() + "/params.ini";

    if ( read_settings( m_sSettingsFile) )    {
        ui->statusbar->showMessage(m_sSettingsFile+" has been read OK");
    }else
        ui->statusbar->showMessage(QApplication::applicationDirPath() + " params.ini settings file not found");
    */
}


void MainWindow::on_pushButton_6_clicked()
{
    theSapex->writeJsonFileInit( QApplication::applicationDirPath() + "/output.json");
}

void MainWindow::on_pushButton_9_clicked() // open json tech rq
{
    if(1){
        QString openFileName = QFileDialog::getOpenFileName(NULL,("Open Json File"),QString(),("JSON (*.json)"));// Choose a file
        theSapex->readJsonFileTechRQ(openFileName);
    }
}

void MainWindow::on_pushButton_5_clicked() //readJsonFileInit
{
    if(1){
        QString openFileName = QFileDialog::getOpenFileName(NULL,("Open Json File"),QString(),("JSON (*.json)"));// Choose a file
        // Create a file object and open it for reading.
        theSapex->readJsonFileInit(openFileName);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
#if 0
    if ( mOpcUaProvider != NULL){
        mOpcUaClient = mOpcUaProvider->createClient("open62541");//mOpcUaPlugin->currentText());
        if (!mOpcUaClient) {
            //g_pOpcUaClient = mOpcUaClient;
            const QString message(tr("Connecting to the given sever failed. See the log for details."));
            //log(message, QString(), Qt::red);
            QMessageBox::critical(this, tr("Failed to connect to server"), message);
            return;
        }else{
            connect(mOpcUaClient, &QOpcUaClient::connected, this, &MainWindow::clientConnected);
            connect(mOpcUaClient, &QOpcUaClient::disconnected, this, &MainWindow::clientDisconnected);
            /*connect(mOpcUaClient, &QOpcUaClient::errorChanged, this, &MainWindow::clientError);
            connect(mOpcUaClient, &QOpcUaClient::stateChanged, this, &MainWindow::clientState);
            connect(mOpcUaClient, &QOpcUaClient::stateChanged, this, &MainWindow::clientState);*/
            const QUrl url("opc.tcp://192.168.8.183:8383");
            mOpcUaClient->connectToEndpoint(url);
        }
    }
#else
    theOpcuaServer->init(MT_opcua_server_url);
#endif

}
void MainWindow::on_pushButton_2_clicked() // disconnect OPCUA
{
#if 0
    mOpcUaClient->disconnectFromEndpoint();
#else
    theOpcuaServer->disconnect();
#endif
}

void MainWindow::on_pushButton_4_clicked() // writeattribute opcua
{
    QString contenu = ui->lineEdit1->text();
#if 0
    QMessageBox::warning(this, tr("on_pushButton_4_clicked"), contenu);
    QString nodeValue = contenu;//.toInt();//"63";
    QVariant value(nodeValue);
    qDebug() << value << " with nodeValue: " << nodeValue;

    QString nodeType = "unsigned char";//QString nodeType = "double";
    const auto type = QVariant::nameToType(nodeType.toLatin1().constData());
    if(type == QVariant::Type::Invalid) {qCritical() << "Invalid node type";return;}

    if(!value.convert(type)) {
        qCritical() << "Value cannot be presented as given type " << nodeType;
        return;
    }
    //const auto node = std::unique_ptr<QOpcUaNode>(mOpcUaClient->node(nodename));

    m_CDE_ORD_PAU = mOpcUaClient->node(QOpcUa::nodeIdFromInteger(6,1410065343));
    if (m_CDE_ORD_PAU)
        m_CDE_ORD_PAU->writeValueAttribute(value);
#else
    theOpcuaServer->NodeWriteValueAttribute(6,1410065343,contenu);
#endif
}

#if 0
void MainWindow::browseFinished(QVector<QOpcUaReferenceDescription> children, QOpcUa::UaStatusCode statusCode)
{

    QMessageBox::warning(this, tr("MainWindow"), "message browseFinished");
    if (statusCode != QOpcUa::Good) {
        qWarning() << "Browsing node" << mOpcNode->nodeId() << "finally failed:" << statusCode;
        return;
    }

    for (const auto &item : children) {
        //if (hasChildNodeItem(item.targetNodeId().nodeId()))            continue;

        //QOpcUaNode * QOpcUaClient::node(const QString &nodeId);
        auto node = mOpcUaClient->node(item.targetNodeId());
        if (!node) {
            qWarning() << "Failed to instantiate node:" << item.targetNodeId().nodeId();
            continue;
        }else{
            qDebug() << "read child node:" << node->nodeId();

            myOpcuaNode* ptr = new myOpcuaNode();
            ptr->m_internalNode = node;
            ptr->m_nodeId = item.targetNodeId().nodeId();
            connect(node, &QOpcUaNode::attributeRead, ptr, &myOpcuaNode::slot_read_attribute);
            node->readAttributes(QOpcUa::NodeAttribute::BrowseName);
            g_object_node_children_map.insert(ptr->m_nodeId, ptr ); // to iterate later
            //qDebug() << "Browse name:" << node->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();
            g_object_node_children_id.insert(ptr->m_nodeId);
        }
        //*mModel->beginInsertRows(index, mChildItems.size(), mChildItems.size() + 1);
        //appendChild(new TreeItem(node, mModel, item, this));
        //mChildNodeIds.insert(item.targetNodeId().nodeId());//child->mNodeId);
        //mModel->endInsertRows();
    }
}

void MainWindow::clientConnected()
{
    //QMessageBox::information(this, tr("MainWindow"), "message clientConnected");
    //mClientConnected = true;
    //updateUiState();
    //mOpcUaModel->setOpcUaClient(mOpcUaClient);
    //mTreeView->header()->setSectionResizeMode(1 /* Value column*/, QHeaderView::Interactive);
    //mOpcUaClient->disconnectFromEndpoint();
    //mRootItem.reset(new TreeItem(client->node("ns=0;i=84"), this /* model */, nullptr /* parent */));
#if 0
    if ( mOpcUaClient != NULL ){
        QOpcUaNode* rootNode;
        rootNode  = mOpcUaClient->node("ns=0;i=85");
        mOpcNode = rootNode;
        //List of All Members for QOpcUaNode
        //This signal is emitted after a browseChildren() or browse() operation has finished.
        connect(rootNode, &QOpcUaNode::browseFinished, this, &MainWindow::browseFinished);
        if (!rootNode->browseChildren())
            qWarning() << "Browsing node" << rootNode->nodeId() << "failed";
        else
            qWarning() << "Browsing node" << rootNode->nodeId() << "OK";
    }
#endif
}

void MainWindow::clientDisconnected()
{
    //QMessageBox::warning(this, tr("MainWindow"), "message clientDisconnected");
    //mClientConnected = false;
#if 0
    mOpcUaClient->deleteLater();
#endif
    //mOpcUaModel->setOpcUaClient(nullptr);
    //updateUiState();
}
#endif

#if 0
void MainWindow::slot3(QOpcUa::NodeAttributes attr)
{
    QMessageBox::warning(this, tr("MainWindow"), "slot3 attributeRead");
    qDebug() << "Signal attributeRead for attributes:" << attr;
    /*if (rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName) != QOpcUa::UaStatusCode::Good)
    {
        qDebug() << "Failed to read attribute:" << rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName);
        mOpcUaClient->disconnectFromEndpoint();
    }*/
    if (m_CDE_ORD_PAU)
    qDebug() << "Browse name:" << m_CDE_ORD_PAU->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();
}


void MainWindow::slot30(QOpcUa::NodeAttributes attr)
{
    //QMessageBox::warning(this, tr("MainWindow"), "slot30 attributeRead");
    qDebug() << "slot30 Signal attributeRead for attributes:" << attr;
    /*
    if (rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName) != QOpcUa::UaStatusCode::Good)
    {
        qDebug() << "Failed to read attribute:" << rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName);
        mOpcUaClient->disconnectFromEndpoint();
    }*/
    //if (m_CDE_ORD_PAU)    qDebug() << "Browse name:" << m_CDE_ORD_PAU->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();

}
#endif

void MainWindow::on_pushButton_3_clicked() // readattribute
{
#if 0
    QString nodename ;    //nodename = "ns=6;s=1410065343";    //nodename = QOpcUa::nodeIdFromString(6,"1410065343");
    nodename = QOpcUa::nodeIdFromInteger(6,1410065343);    //const auto node = std::unique_ptr<QOpcUaNode>(mOpcUaClient->node(nodename));
    m_CDE_ORD_PAU = mOpcUaClient->node(nodename);
    if(!m_CDE_ORD_PAU) {qCritical() << "No such node";return; }else qCritical() << "FOUND node";

    connect(m_CDE_ORD_PAU, &QOpcUaNode::attributeRead, this, &MainWindow::slot3);
    if (!m_CDE_ORD_PAU->readAttributes( QOpcUa::NodeAttribute::Value
                            | QOpcUa::NodeAttribute::NodeClass
                            | QOpcUa::NodeAttribute::Description
                            | QOpcUa::NodeAttribute::DataType
                            | QOpcUa::NodeAttribute::BrowseName
                            | QOpcUa::NodeAttribute::DisplayName
                            ))
        qWarning() << "Reading attributes" << m_CDE_ORD_PAU->nodeId() << "failed";
#else
    QString contenu;
    theOpcuaServer->NodeReadValueAttribute(6,1410065343,contenu);
#endif
}

#if 0
void MainWindow::slot6(QOpcUa::NodeAttribute attr, const QVariant &value)
{
    Q_UNUSED(attr);
    QString str=  QString("QVariant integer:%1").arg(value.toUInt());
    QMessageBox::information(this, tr("enableMonitoring"), QString("QOpcUa::NodeAttribute::Value=")+str);//
}
#endif

void MainWindow::on_pushButton_10_clicked()
{
#if 0
    PAU_node* a_PAU_node = new PAU_node;
    if(a_PAU_node!=NULL){
            a_PAU_node->monitor_FONC(mOpcUaClient);
        }else{
            QString contenu = ui->lineEdit1->text();
            QMessageBox::warning(this, tr("on_pushButton_4_clicked"), contenu);
            a_PAU_node->set_FONC(mOpcUaClient,contenu);
        }
#endif
}

void MainWindow::on_pushButton_7_clicked() // monitorattributechange
{
#if 0
    const QString message(tr("subscriptionTest..."));
    //QString nodename ;    //nodename = "ns=6;s=1410065343";    //nodename = QOpcUa::nodeIdFromString(6,"1410065343");
    QString nodename = QOpcUa::nodeIdFromInteger(6,1410065343);    //const auto node = std::unique_ptr<QOpcUaNode>(mOpcUaClient->node(nodename));
    m_CDE_ORD_PAU = mOpcUaClient->node(nodename);
    /*QOpcUaMonitoringParameters p(0);
    QOpcUaMonitoringParameters::EventFilter filter;
    filter << QOpcUa::QSimpleAttributeOperand("Severity");
    filter << QOpcUa::QSimpleAttributeOperand("Message");
    p.setFilter(filter);
    m_CDE_ORD_PAU->enableMonitoring(QOpcUa::NodeAttribute::EventNotifier, p);*/

    connect(m_CDE_ORD_PAU, &QOpcUaNode::dataChangeOccurred, this, &MainWindow::slot6);

    QMessageBox::information(this, tr("subscriptionTest"), "enableMonitoring");
    //QOpcUaMonitoringParameters p(1000, QOpcUaMonitoringParameters::SubscriptionType::Exclusive);
    m_CDE_ORD_PAU->enableMonitoring(QOpcUa::NodeAttribute::Value,QOpcUaMonitoringParameters(300));
    // Set a publishing interval of 100ms and share the subscription.
    //quint32 subscriptionId = m_CDE_ORD_PAU->monitoringStatus(QOpcUa::NodeAttribute::Value).subscriptionId();
    //qDebug() << subscriptionId << " <-id : Monitoring enabled, waiting for event...";
#else
    QString contenu;
    //theOpcuaServer->NodeMonitorAttribute(6,1410065343,contenu);
#endif

}


void MainWindow::on_pushButton_8_clicked()
{

}


void MainWindow::on_pushButton_11_clicked()  // browse PAU
{
    QMap<QString, myOpcuaNode*>::iterator i;
    for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i)
    {
        myOpcuaNode* ptr_node = i.value();
        qDebug() << "key:"<<i.key() << " node name: " << ptr_node->m_nodename ;//<< Qt::endl;
/*
key: "ns=6;i=80100"  node name:  "PAU_04_1_00045"
key: "ns=6;i=8100"  node name:  "PAU_04_1_00008"
key: "ns=6;i=8200"  node name:  "PAU_04_1_00009"
key: "ns=6;i=900"  node name:  "PAU_01_1_00034"
key: "ns=6;i=90100"  node name:  "PAU_04_1_00046"
key: "ns=6;i=9500"  node name:  "PAU_04_1_00010"
key: "ns=6;i=9600"  node name:  "PAU_04_1_00011"
key: "ns=6;i=9700"  node name:  "PAU_04_1_00012"
key: "ns=6;i=9800"  node name:  "PAU_04_1_00013"
key: "ns=6;i=9900"  node name:  "PAU_04_1_00014"
key: "ns=7;i=30100"  node name:  "IHM_00001"
key: "ns=7;i=40100"  node name:  "IHM_00002"
key: "ns=7;i=50100"  node name:  "IHM_00003"
key: "ns=7;i=60100"  node name:  "IHM_00004"
key: "ns=7;i=70100"  node name:  "IHM_00005"
key: "ns=8;i=301"  node name:  "PHONE_00001"
key: "ns=8;i=401"  node name:  "PHONE_00002"
key: "ns=8;i=501"  node name:  "PHONE_00003"
key: "ns=8;i=601"  node name:  "PHONE_00004"
key: "ns=8;i=701"  node name:  "PHONE_00005"*/

        if (ptr_node->m_nodename.contains("PAU_") )
            m_plist->insertItem(0, QString("%1|%2").arg(i.key() ).arg(ptr_node->m_nodename) );

        /*if ( i.key() == "ns=6;i=1410065308" ) // PAU_00_0_00000
        {
            //ptr_node->browseChildren();
        }*/
    }
}

void MainWindow::on_pushButton_12_clicked() // show children
{

}

void MainWindow::on_listWidget1_itemDoubleClicked(QListWidgetItem *item)
{
    qDebug() << item->text();
    QString str = item->text();
    QStringList list = str.split("|");
    qDebug() << list;
    if (list.count() == 2)
    {
        QString ns_id = list.at(0);
        QString nodename = list.at(1);
        if(0){
            myOpcuaNode* ptr_node = theOpcuaServer->findNodeByNodeId(ns_id);
            if (ptr_node){
                current_node = ptr_node;
                qDebug() << "FOUND " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                m_plist2->insertItem(0, QString("children current node %1 %2").arg(ptr_node->m_nodeId ).arg(ptr_node->m_nodename) );
                ptr_node->browseChildren();
                return;
            }

        }else{

            myOpcuaNode* ptr_node = theOpcuaServer->findNodeByName(nodename);
            if (ptr_node){
                current_node = ptr_node;
                qDebug() << "FOUND " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                m_plist2->insertItem(0, QString("children current node %1 %2").arg(ptr_node->m_nodeId ).arg(ptr_node->m_nodename) );
                ptr_node->browseChildren();
                return;
            }

            /*QMap<QString, myOpcuaNode*>::iterator i;
            for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i) {
                myOpcuaNode* ptr_node = i.value();
                //if ( ns_id == "ns=6;i=1410065308" && ptr_node->m_nodeId == ns_id ) // PAU_00_0_00000
                if ( ptr_node->m_nodeId == ns_id ) // celui sur lequel on click
                {
                    current_node = ptr_node;
                    qDebug() << "FOUND " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                    m_plist2->insertItem(0, QString("children current node %1 %2").arg(ptr_node->m_nodeId ).arg(ptr_node->m_nodename) );
                    ptr_node->browseChildren();
                    return;
                }
            }*/
        }
    }

}

void MainWindow::on_listWidget2_itemClicked(QListWidgetItem *item)
{
    if ( current_node !=  NULL)
    {
        qDebug() << item->text();
        QString str = item->text();
        QStringList list = str.split("|");
        qDebug() << list;
        if (list.count() == 2)
        {
            QString ns_id = list.at(0);
            QString nodename = list.at(1);
            QMap<QString, myOpcuaNode*>::iterator i;
            for (i = current_node->m_subnodes.begin(); i != current_node->m_subnodes.end(); ++i) {
                myOpcuaNode* ptr_node = i.value();
                if ( ptr_node->m_nodeId == ns_id ){
                    myOpcuaNode* ptr_node = i.value();
                    //m_plist2->insertItem(0, QString("%1|%2").arg(i.key() ).arg(i.value()->m_nodename) );
                    qDebug() << " list2 clicked on "<< QString("%1|%2").arg(i.key() ).arg(i.value()->m_nodename) ;
                    QVariant vv =  ptr_node->m_internalNode->valueAttribute() ;// ptr_node->valueAttribute();//readValueAttribute();
                    //ptr_node->writeValueAttribute("44");
                    qDebug() << vv;
                }
            }
        }
    }
}


void MainWindow::on_listWidget2_itemDoubleClicked(QListWidgetItem *item)
{
    if ( current_node !=  NULL)
    {
        qDebug() << item->text();
        QString str = item->text();
        QStringList list = str.split("|");
        qDebug() << list;
        if (list.count() == 2)
        {
            QString ns_id = list.at(0);
            QString nodename = list.at(1);
            QMap<QString, myOpcuaNode*>::iterator i;
            for (i = current_node->m_subnodes.begin(); i != current_node->m_subnodes.end(); ++i) {
                myOpcuaNode* ptr_node = i.value();
                if ( ptr_node->m_nodeId == ns_id ){
                    myOpcuaNode* ptr_node = i.value();
                    //m_plist2->insertItem(0, QString("%1|%2").arg(i.key() ).arg(i.value()->m_nodename) );
                    qDebug() << " try monitoring "<< QString("%1|%2").arg(i.key() ).arg(i.value()->m_nodename) ;
                    ptr_node->monitorAttribute();
                }
            }
        }
    }
}

void MainWindow::on_pushButton_13_clicked() // refresh
{
#if 1
    if ( current_node !=  NULL)
    {
        QMap<QString, myOpcuaNode*>::iterator i;
        for (i = current_node->m_subnodes.begin(); i != current_node->m_subnodes.end(); ++i) {
            //myOpcuaNode* ptr_node = i.value();
            m_plist2->insertItem(0, QString("%1|%2").arg(i.key() ).arg(i.value()->m_nodename) );
        }
    }
#else

#endif
}

