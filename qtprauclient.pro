#TARGET = xcsta_rec_console

DEFINES += WITH_GUI
DEFINES += BUILD_DATE='"$(shell date)"'

QT       += core

contains ( DEFINES, WITH_GUI ){
    LIBS += -lcrypto -lssl
    QT       += gui
    QT       += core gui
    greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
    #CONFIG   += console
    #CONFIG   -= app_bundle
} else {
    LIBS += -lcrypto
}


LIBS+= /opt/qt5/5.15.2/gcc_64/lib/libQt5OpcUa.so


CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    jsoncommand.cpp \
    main.cpp \
    mainwindow.cpp \
    mtnode.cpp \
    mtserver.cpp \
    pau.cpp \
    sapex.cpp \
    utils.cpp

HEADERS += \
    globales.h \
    mainwindow.h \
    mtserver.h \
    sapex.h \
    utils.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
