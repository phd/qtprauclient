#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QMutex>
#include <QTimer>

#include "mtserver.h"
#include "globales.h"

void myOpcuaServer::disconnect()
{
    m_rootNode = NULL;
    mOpcUaClient->disconnectFromEndpoint();
}

void myOpcuaServer::init(QString url) //const QUrl url("opc.tcp://192.168.8.183:8383");
{
    m_rootNode = NULL;
    if ( mOpcUaProvider == NULL)
        mOpcUaProvider = new QOpcUaProvider(this);
    if ( mOpcUaProvider != NULL){
        mOpcUaClient = mOpcUaProvider->createClient("open62541");//mOpcUaPlugin->currentText());
        if (!mOpcUaClient) {
            //g_pOpcUaClient = mOpcUaClient;
            const QString message(tr("Connecting to the given sever failed. See the log for details."));
            //log(message, QString(), Qt::red);
            //QMessageBox::critical(NULL, tr("Failed to connect to server"), "myOpcuaServer" );
            return;
        }else{
            connect(mOpcUaClient, &QOpcUaClient::connected, this, &myOpcuaServer::slot_clientConnected);
            connect(mOpcUaClient, &QOpcUaClient::disconnected, this, &myOpcuaServer::slot_clientDisconnected);
            /*connect(mOpcUaClient, &QOpcUaClient::errorChanged, this, &MainWindow::clientError);
            connect(mOpcUaClient, &QOpcUaClient::stateChanged, this, &MainWindow::clientState);
            connect(mOpcUaClient, &QOpcUaClient::stateChanged, this, &MainWindow::clientState);*/
            mOpcUaClient->connectToEndpoint(url);
        }
    }
}

myOpcuaNode* myOpcuaServer::findNodeByName(QString name)
{
    QMap<QString, myOpcuaNode*>::iterator i;
    for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i) {
        myOpcuaNode* ptr_node = i.value();
        //if ( ns_id == "ns=6;i=1410065308" && ptr_node->m_nodeId == ns_id ) // PAU_00_0_00000
        if ( ptr_node->m_nodename == name )
        {
            //current_node = ptr_node;
            qDebug() << "myOpcuaServer::findNodeByName " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
            return ptr_node;
        }
    }
    return NULL;
}

myOpcuaNode* myOpcuaServer::findNodeByNodeId(QString nodeid)
{
    QMap<QString, myOpcuaNode*>::const_iterator i = g_object_node_children_map.find(nodeid);
    if (i != g_object_node_children_map.end()) {
        myOpcuaNode* ptr_node = i.value();
        return ptr_node;
    }
    return NULL;
}


void myOpcuaServer::slot_clientDisconnected()
{
    //QMessageBox msgBox;msgBox.setText("myOpcuaServer::clientDisconnected");msgBox.exec();
    qDebug() << "myOpcuaServer::slot_clientDisconnected ";
    m_ClientConnected = false;
    mOpcUaClient->deleteLater();
}


void myOpcuaServer::slot_clientConnected()
{
    //QMessageBox msgBox;msgBox.setText("myOpcuaServer::clientConnected");msgBox.exec();
    qDebug() << "myOpcuaServer::slot_clientConnected ";
    m_ClientConnected = true;
    browseObjectNodeChildren();
}

void myOpcuaServer::enableMonitoringTag()
{
    m_tag_list.append(QString("CDE_ORD_PAU"));
    usleep(ONE_SEC_USLEEP * 1);
    MT_Event* pev = new MT_Event;
    if (pev){

        QMap<QString, myOpcuaNode*>::iterator i;//foreach (const QJsonValue & v, jsarr)qDebug() << v.toObject().value("ID").toInt();
        for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i)
        {
            myOpcuaNode* ptr_node = i.value();//if ( ns_id == "ns=6;i=1410065308" && ptr_node->m_nodeId == ns_id ) // PAU_00_0_00000

            if ( ptr_node->m_nodename.contains("PAU_") && m_list_PAU.contains(ptr_node->m_nodename) )
            {
                qDebug() << "myOpcuaServer::sendINITresponse " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                pev->m_PAU_Tag_list.append(ptr_node->m_nodename);

                QMap<QString, myOpcuaNode*>::iterator i;
                for (i = ptr_node->m_subnodes.begin(); i != ptr_node->m_subnodes.end(); ++i) {
                    myOpcuaNode* ptr_snode = i.value();
                    if ( m_tag_list.contains(ptr_snode->m_nodename))
                    {
                         qDebug() << "MONITOR subnode " << ptr_snode->m_nodename << " variant="<< ptr_snode->m_variant;
                         ptr_snode->monitorAttribute();
                    }else
                        qDebug() << "subnode " << ptr_snode->m_nodename << " variant="<< ptr_snode->m_variant;
                }
            }
        }
    }
}

void myOpcuaServer::sendINITresponse()
{
    usleep(ONE_SEC_USLEEP * 1);
    MT_Event* pev = new MT_Event;
    if (pev){

        QMap<QString, myOpcuaNode*>::iterator i;//foreach (const QJsonValue & v, jsarr)qDebug() << v.toObject().value("ID").toInt();
        for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i)
        {
            myOpcuaNode* ptr_node = i.value();//if ( ns_id == "ns=6;i=1410065308" && ptr_node->m_nodeId == ns_id ) // PAU_00_0_00000

            if ( ptr_node->m_nodename.contains("PAU_") && m_list_PAU.contains(ptr_node->m_nodename) )
            {
                qDebug() << "myOpcuaServer::sendINITresponse " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                pev->m_PAU_Tag_list.append(ptr_node->m_nodename);

                QMap<QString, myOpcuaNode*>::iterator i;
                for (i = ptr_node->m_subnodes.begin(); i != ptr_node->m_subnodes.end(); ++i) {
                    myOpcuaNode* ptr_snode = i.value();
                    qDebug() << "subnode " << ptr_snode->m_nodeId << "FOUND " << ptr_snode->m_nodename << " variant:"<< ptr_snode->m_variant;
                }
            }
        }
        pev->m_eventType = MT_Event::MT_INIT_REPONSE ;
        theBridge->_eventsReceivedFromMT.push(pev);        /*
myOpcuaServer::sendINITresponse  "ns=6;i=1410065308" FOUND  "PAU_00_0_00000"
subnode  "ns=6;i=1410065309" FOUND  "AAL"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065310" FOUND  "AT1"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065311" FOUND  "AT2"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065312" FOUND  "CO1"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065313" FOUND  "CO2"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065314" FOUND  "DAC"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065315" FOUND  "DAL"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065316" FOUND  "DTR"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065317" FOUND  "EA1"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065318" FOUND  "ESA"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065319" FOUND  "IPS"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065320" FOUND  "ISS"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065321" FOUND  "NOF"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065322" FOUND  "OCC"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065323" FOUND  "OP1"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065324" FOUND  "OP2"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065325" FOUND  "PHB"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065326" FOUND  "PHO"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065327" FOUND  "RAD"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065328" FOUND  "RPF"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065329" FOUND  "RSA"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065330" FOUND  "RSF"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065331" FOUND  "SEC"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065332" FOUND  "SEQ"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065333" FOUND  "TON"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065334" FOUND  "TRA"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065341" FOUND  "ALERTE_APPEL"  variant: QVariant(bool, false)
subnode  "ns=6;i=1410065342" FOUND  "AUDIO_REC"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065343" FOUND  "CDE_ORD_PAU"  variant: QVariant(uchar, 46)
subnode  "ns=6;i=1410065344" FOUND  "CDE_REG_OPE"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065345" FOUND  "ETAT_PAU"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=1410065346" FOUND  "OP_EN_COM"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065347" FOUND  "POS_ANT_PIR"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065348" FOUND  "POS_ANT_SECU"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=1410065349" FOUND  "POS_AXE"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065350" FOUND  "POS_HORODATE_GPS"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065351" FOUND  "POS_LAT"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065352" FOUND  "POS_LOC_PIR"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065353" FOUND  "POS_LONG"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065354" FOUND  "POS_MOBILE"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=1410065355" FOUND  "POS_NUM_TEL"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065356" FOUND  "POS_PRMETRE"  variant: QVariant(uint, 0)
subnode  "ns=6;i=1410065357" FOUND  "POS_REPERE"  variant: QVariant(QString, "")
subnode  "ns=6;i=1410065358" FOUND  "POS_SENS"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=1410065359" FOUND  "STA_PAU"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=1410065360" FOUND  "TC_PILOTER_PAU"  variant: QVariant(bool, false)
myOpcuaServer::sendINITresponse  "ns=6;i=9600" FOUND  "PAU_04_1_00011"
subnode  "ns=6;i=9601" FOUND  "AAL"  variant: QVariant(bool, false)
subnode  "ns=6;i=9602" FOUND  "AT1"  variant: QVariant(bool, false)
subnode  "ns=6;i=9603" FOUND  "AT2"  variant: QVariant(bool, false)
subnode  "ns=6;i=9604" FOUND  "CO1"  variant: QVariant(bool, false)
subnode  "ns=6;i=9605" FOUND  "CO2"  variant: QVariant(bool, false)
subnode  "ns=6;i=9606" FOUND  "DAC"  variant: QVariant(bool, false)
subnode  "ns=6;i=9607" FOUND  "DAL"  variant: QVariant(bool, false)
subnode  "ns=6;i=9608" FOUND  "DTR"  variant: QVariant(bool, false)
subnode  "ns=6;i=9609" FOUND  "EA1"  variant: QVariant(bool, false)
subnode  "ns=6;i=9610" FOUND  "ESA"  variant: QVariant(bool, false)
subnode  "ns=6;i=9611" FOUND  "IPS"  variant: QVariant(bool, false)
subnode  "ns=6;i=9612" FOUND  "ISS"  variant: QVariant(bool, false)
subnode  "ns=6;i=9613" FOUND  "NOF"  variant: QVariant(bool, false)
subnode  "ns=6;i=9614" FOUND  "OCC"  variant: QVariant(bool, false)
subnode  "ns=6;i=9615" FOUND  "OP1"  variant: QVariant(bool, false)
subnode  "ns=6;i=9616" FOUND  "OP2"  variant: QVariant(bool, false)
subnode  "ns=6;i=9617" FOUND  "PHB"  variant: QVariant(bool, false)
subnode  "ns=6;i=9618" FOUND  "PHO"  variant: QVariant(bool, false)
subnode  "ns=6;i=9619" FOUND  "RAD"  variant: QVariant(bool, false)
subnode  "ns=6;i=9620" FOUND  "RPF"  variant: QVariant(bool, false)
subnode  "ns=6;i=9621" FOUND  "RSA"  variant: QVariant(bool, false)
subnode  "ns=6;i=9622" FOUND  "RSF"  variant: QVariant(bool, false)
subnode  "ns=6;i=9623" FOUND  "SEC"  variant: QVariant(bool, false)
subnode  "ns=6;i=9624" FOUND  "SEQ"  variant: QVariant(bool, false)
subnode  "ns=6;i=9625" FOUND  "TON"  variant: QVariant(bool, false)
subnode  "ns=6;i=9626" FOUND  "TRA"  variant: QVariant(bool, false)
subnode  "ns=6;i=9633" FOUND  "ALERTE_APPEL"  variant: QVariant(bool, false)
subnode  "ns=6;i=9634" FOUND  "AUDIO_REC"  variant: QVariant(QString, "")
subnode  "ns=6;i=9635" FOUND  "CDE_ORD_PAU"  variant: QVariant(uchar, 0)
subnode  "ns=6;i=9636" FOUND  "CDE_REG_OPE"  variant: QVariant(QString, "")
subnode  "ns=6;i=9637" FOUND  "ETAT_PAU"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=9638" FOUND  "OP_EN_COM"  variant: QVariant(QString, "")
subnode  "ns=6;i=9639" FOUND  "POS_ANT_PIR"  variant: QVariant(QString, "")
subnode  "ns=6;i=9640" FOUND  "POS_ANT_SECU"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=9641" FOUND  "POS_AXE"  variant: QVariant(QString, "")
subnode  "ns=6;i=9642" FOUND  "POS_HORODATE_GPS"  variant: QVariant(QString, "")
subnode  "ns=6;i=9643" FOUND  "POS_LAT"  variant: QVariant(QString, "")
subnode  "ns=6;i=9644" FOUND  "POS_LOC_PIR"  variant: QVariant(QString, "")
subnode  "ns=6;i=9645" FOUND  "POS_LONG"  variant: QVariant(QString, "")
subnode  "ns=6;i=9646" FOUND  "POS_MOBILE"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=9647" FOUND  "POS_NUM_TEL"  variant: QVariant(QString, "")
subnode  "ns=6;i=9648" FOUND  "POS_PRMETRE"  variant: QVariant(uint, 0)
subnode  "ns=6;i=9649" FOUND  "POS_REPERE"  variant: QVariant(QString, "")
subnode  "ns=6;i=9650" FOUND  "POS_SENS"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=9651" FOUND  "STA_PAU"  variant: QVariant(ushort, 0)
subnode  "ns=6;i=9652" FOUND  "TC_PILOTER_PAU"  variant: QVariant(bool, false)*/
    }
}

void myOpcuaServer::browseObjectNodeChildren()
{
    if ( mOpcUaClient != NULL && m_ClientConnected ){
        m_rootNode  = mOpcUaClient->node(__NODEID_OPCUA_OBJECTS_ROOT__);
        if (!m_rootNode) qWarning() << "Failed to instantiate __NODEID_OPCUA_OBJECTS_ROOT__" ;
        else{
            //This signal is emitted after a browseChildren() or browse() operation has finished.
            connect(m_rootNode, &QOpcUaNode::browseFinished, this, &myOpcuaServer::slot_browseFinished);
            if (!m_rootNode->browseChildren()) qWarning() << "Browsing node" << m_rootNode->nodeId() << "failed";
            else qWarning() << "Browsing node" << m_rootNode->nodeId() << "OK";
        }
    }
}

void myOpcuaServer::slot_browseFinished(QVector<QOpcUaReferenceDescription> children, QOpcUa::UaStatusCode statusCode)
{
    //QMessageBox msgBox;msgBox.setText("myOpcuaServer::browseFinished");msgBox.exec();
    if (statusCode != QOpcUa::Good) {
        qWarning() << "myOpcuaServer::slot_browseFinished:" << m_rootNode->nodeId() << "finally failed:" << statusCode;
        return;
    }else qDebug() << "myOpcuaServer::slot_browseFinished:" << m_rootNode->nodeId() << "finally OK:" << statusCode;
    for (const auto &item : children) {
        myOpcuaNode* ptr = NULL;//findNodeByNodeId(item.targetNodeId().nodeId());
        if ( ptr==NULL){
            auto node = mOpcUaClient->node(item.targetNodeId());//QOpcUaNode * QOpcUaClient::node(const QString &nodeId);
            if (!node) {
                qWarning() << "Failed to instantiate node:" << item.targetNodeId().nodeId();
            }else{
                qDebug() << "read child node:" << node->nodeId();
                ptr = new myOpcuaNode();
                ptr->m_internalNode = node;
                ptr->m_nodeId = item.targetNodeId().nodeId();
                g_object_node_children_map.insert(ptr->m_nodeId, ptr ); // to iterate later
                ptr->readValueAttribute();
            }
        }else{
            qDebug() << "ALREADY HERE node nodeId:" << item.targetNodeId().nodeId();
        }
    }    
    // timer pour recolter tous les PAUs et leur TAGs puis envoyer le resultat au SAPEX
    //QTimer::singleShot(3000, this, &myOpcuaServer::browseObjectNodeChildrenSubnodes);
}

void myOpcuaServer::browseObjectNodeChildrenSubnodes()
{
    if ( m_list_PAU.count() == 0 ) return;
    if ( g_object_node_children_map.count() == 0 ) return;

    QMap<QString, myOpcuaNode*>::iterator i;
    for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i)
    {
        myOpcuaNode* ptr_node = i.value();
        QString ns_id = i.key();//browseObjectNodeChildrenSubnodes key: "ns=6;i=9600"  node name:  "PAU_04_1_00011"
        //qDebug() << "browseObjectNodeChildrenSubnodes key:"<<i.key() << " node name: " << ptr_node->m_nodename ;qDebug() << m_list_PAU;

        if ( ptr_node->m_nodename.contains("PAU_") && m_list_PAU.contains(ptr_node->m_nodename) )
        {
            //myOpcuaNode* ptr_snode = theOpcuaServer->findNodeByNodeId(ptr_node->m_nodeId); //OK
            //myOpcuaNode* ptr_snode = theOpcuaServer->findNodeByName(ptr_node->m_nodename); //OK
            //if (ptr_snode)
            {
                qDebug() << "browseObjectNodeChildrenSubnodes key:"<<i.key() << " node name: " << ptr_node->m_nodename ;//qDebug() << m_list_PAU;
                qDebug() << "browseChildren of PAU: " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                //m_plist2->insertItem(0, QString("children current node %1 %2").arg(ptr_node->m_nodeId ).arg(ptr_node->m_nodename) );
                ptr_node->browseChildren();
                while(ptr_node->m_bowsing) {
                    qDebug() << "WAIT browseChildren of PAU: " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                    usleep(ONE_SEC_USLEEP);
                }
                qDebug() << "DONE browseChildren of PAU: " << ptr_node->m_nodeId << "FOUND " << ptr_node->m_nodename;
                /*
                myOpcuaNode::slot_attribute_read: "AUDIO_REC"
                myOpcuaNode::slot_attribute_read: "CDE_ORD_PAU"
                myOpcuaNode::slot_attribute_read: "CDE_REG_OPE"
                myOpcuaNode::slot_attribute_read: "ETAT_PAU"
                myOpcuaNode::slot_attribute_read: "OP_EN_COM"
                myOpcuaNode::slot_attribute_read: "POS_ANT_PIR"
                myOpcuaNode::slot_attribute_read: "POS_ANT_SECU"
                myOpcuaNode::slot_attribute_read: "POS_AXE"
                myOpcuaNode::slot_attribute_read: "POS_HORODATE_GPS"
                myOpcuaNode::slot_attribute_read: "POS_LAT"
                myOpcuaNode::slot_attribute_read: "POS_LOC_PIR"*/
            }
        }
    }
    QTimer::singleShot(2000, this, &myOpcuaServer::enableMonitoringTag);
//    QTimer::singleShot(3000, this, &myOpcuaServer::sendINITresponse);
}

void myOpcuaServer::NodeWriteValueAttribute(int ns, int id_node, QString nodeValue) //QOpcUa::nodeIdFromInteger(6,1410065343) , value )
{
    QVariant value(nodeValue);
    qDebug() << value << " with nodeValue: " << nodeValue;
    QString nodeType = "unsigned char";//QString nodeType = "double";
    const auto type = QVariant::nameToType(nodeType.toLatin1().constData());
    if(type == QVariant::Type::Invalid) {qCritical() << "Invalid node type";return;}
    if(!value.convert(type)) {
        qCritical() << "Value cannot be presented as given type " << nodeType;
        return;
    }
    QOpcUaNode* m_CDE_ORD_PAU = mOpcUaClient->node(QOpcUa::nodeIdFromInteger(ns,id_node));
    if (m_CDE_ORD_PAU)
        m_CDE_ORD_PAU->writeValueAttribute(value);
}

void myOpcuaServer::NodeReadValueAttribute(int ns, int id_node, QString& nodeValue) //QOpcUa::nodeIdFromInteger(6,1410065343) , value )
{
    if (1)
    {
        qDebug() << "myOpcuaServer::NodeReadValueAttribute:" ;
        //extern QMap<QString ,myOpcuaNode*>     g_object_node_children_map;
        // iterate a qmap
        //g_object_node_children_map.insert(ptr->m_nodeId, ptr ); // to iterate later
        QMap<QString, myOpcuaNode*>::iterator i;
        for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i)
        {
            myOpcuaNode* ptr_node = i.value();
            qDebug() << "key:"<<i.key() << " node name: " << ptr_node->m_nodename << Qt::endl;
            ptr_node->readValueAttribute();
        }
    }else{
#if 0
        QString nodename ;    //nodename = "ns=6;s=1410065343";    //nodename = QOpcUa::nodeIdFromString(6,"1410065343");
        nodename = QOpcUa::nodeIdFromInteger(ns,id_node);//6,1410065343);    //const auto node = std::unique_ptr<QOpcUaNode>(mOpcUaClient->node(nodename));
        current_node = mOpcUaClient->node(nodename);

        if(!current_node) {qCritical() << "No such node";return; }else qCritical() << "FOUND node";

        connect(current_node, &QOpcUaNode::attributeRead, this, &myOpcuaServer::slot_attribute_read);
        if (!current_node->readAttributes( QOpcUa::NodeAttribute::Value
                                           | QOpcUa::NodeAttribute::NodeClass
                                           | QOpcUa::NodeAttribute::Description
                                           | QOpcUa::NodeAttribute::DataType
                                           | QOpcUa::NodeAttribute::BrowseName
                                           | QOpcUa::NodeAttribute::DisplayName
                                           ))
            qWarning() << "Reading attributes" << current_node->nodeId() << "failed";
#endif
    }
}

#if 0
void myOpcuaServer::slot_attribute_read(QOpcUa::NodeAttributes attr)
{
    QMessageBox msgBox;msgBox.setText("myOpcuaServer::slot_attribute_read");msgBox.exec();
    qDebug() << "Signal attributeRead for attributes:" << attr;
    /*if (rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName) != QOpcUa::UaStatusCode::Good)
    {
        qDebug() << "Failed to read attribute:" << rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName);
        mOpcUaClient->disconnectFromEndpoint();
    }*/
    if (current_node)
    qDebug() << "Browse name:" << current_node->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();
}
#endif
#if 0
void myOpcuaNode::slot_browse_read_attribute(QOpcUa::NodeAttributes attr)
{
    //QMessageBox::warning(this, tr("MainWindow"), "slot30 attributeRead");
    m_nodename = m_internalNode->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();
    qDebug() << m_nodeId << " Signal attributeRead for attributes:" << attr;
    qDebug() << m_nodename << " Signal attributeRead for attributes:" << attr;

    //qDebug() << "Browse name:" << node->attribute(            QOpcUa::NodeAttribute::BrowseName            ).value<QOpcUa::QQualifiedName>().name();
    /*
    if (rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName) != QOpcUa::UaStatusCode::Good)
    {
        qDebug() << "Failed to read attribute:" << rootNode->attributeError(QOpcUa::NodeAttribute::BrowseName);
        mOpcUaClient->disconnectFromEndpoint();
    }*/
    //if (m_CDE_ORD_PAU)    qDebug() << "Browse name:" << m_CDE_ORD_PAU->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();

}

void myOpcuaServer::slot_monitoredNodeDataChangeOccurred(QOpcUa::NodeAttribute attr, const QVariant &value)
{
    Q_UNUSED(attr);
    QString str=  QString("QVariant integer:%1").arg(value.toUInt());
    QMessageBox msgBox;msgBox.setText(QString("QOpcUa::NodeAttribute::Value=")+str);msgBox.exec();
    //QMessageBox::information(this, tr("enableMonitoring"), QString("QOpcUa::NodeAttribute::Value=")+str);//
}


void myOpcuaServer::NodeMonitorAttribute(int ns, int id_node, QString& nodeValue) //QOpcUa::nodeIdFromInteger(6,1410065343) , value )
{
    if (0){
        QMap<QString, myOpcuaNode*>::iterator i;
        for (i = g_object_node_children_map.begin(); i != g_object_node_children_map.end(); ++i)
        {
            myOpcuaNode* ptr_node = i.value();
            qDebug() << "key:"<<i.key() << " node name: " << ptr_node->m_nodename ;//<< Qt::endl;
            //if ( i.key() == "ns=6;i=1410065343" )
            if ( i.key() == "ns=6;i=1410065308" ) // PAU_00_0_00000
            {
                //ptr_node->browseChildren();
            }
        }
    }else{
        QString nodename ;    //nodename = "ns=6;s=1410065343";    //nodename = QOpcUa::nodeIdFromString(6,"1410065343");
        nodename = QOpcUa::nodeIdFromInteger(ns,id_node);//6,1410065343);    //const auto node = std::unique_ptr<QOpcUaNode>(mOpcUaClient->node(nodename));
        current_monitored_node = mOpcUaClient->node(nodename);

        connect(current_monitored_node, &QOpcUaNode::dataChangeOccurred, this, &myOpcuaServer::slot_monitoredNodeDataChangeOccurred);
        current_monitored_node->enableMonitoring(QOpcUa::NodeAttribute::Value,QOpcUaMonitoringParameters(300));
    }
}
#endif





