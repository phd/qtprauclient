#ifndef UTILS_H
#define UTILS_H

#if 0
#include <QMainWindow>
#include <QListView>
#include <QLineEdit>
#include <QListWidget>
#include <QtOpcUa/QOpcUaClient>
#include <QtOpcUa/QOpcUaProvider>

#include <unistd.h>     /* for close() */
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <limits.h>
#include <arpa/inet.h> //inet_addr
#include <sys/socket.h>    //socket
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <arpa/inet.h> // for inet_ntoa()
#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip_icmp.h>   //Provides declarations for icmp header
#include <netinet/udp.h> //Provides declarations for udp header
#include <netinet/tcp.h> //Provides declarations for tcp header
#include <netinet/ip.h>  //Provides declarations for ip header
#include <arpa/inet.h> //inet_addr
#include <QMap>
#include <QString>


#endif

#include <QQueue>
#include <QMutex>
#include <QThread>
#include <QCoreApplication>

template<class T> class QAsyncQueue
{
public:
    QAsyncQueue(uint _max = -1) : _max(_max){}
    void setMax(int mm){
        _max = mm; // -1 : infinite
    }
    ~QAsyncQueue()    {
        clean();
    }
    uint count(){
        _mutex.lock();
        int count = _queue.count();
        _mutex.unlock();
        return count;
    }
    bool isFull(){
        if (-1 == _max)
            return false;

        _mutex.lock();
        int count = _queue.count();
        _mutex.unlock();
        return count >= _max;
    }
    bool isEmpty(){
        _mutex.lock();
        bool empty = _queue.isEmpty();
        _mutex.unlock();
        return empty;
    }
    void clean(){
        _mutex.lock();
        _queue.clear();
        _mutex.unlock();
    }
    void push(const T& t){
        _mutex.lock();
        _queue.enqueue(t);
        _mutex.unlock();
    }
    T pull(){
        _mutex.lock();
        T i = _queue.dequeue();
        _mutex.unlock();
        return i;
    }
private:
    QQueue<T> _queue;
    QMutex _mutex;
    int _max;
};

#endif // UTILS_H
