#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QMutex>

#include "globales.h"


void SAPEX::init(QString addr, uint port)
{
    _is_stopped = false;
    _sapex_ip_addr = addr;
    _sapex_port = port;
    memset(&_sapex_server_sock,0,sizeof(_sapex_server_sock)); //struct sockaddr_in
    _sapex_server_sock.sin_addr.s_addr = inet_addr(_sapex_ip_addr.toLatin1().constData());
    _sapex_server_sock.sin_family = AF_INET;
    _sapex_server_sock.sin_port = htons( _sapex_port );

    pthread_create(&_thread_id, NULL, SAPEX::SapexListeningThread, (void*)this);
}

void SAPEX::sendJSONtoSAPEX(MT_Event* ev)
{
    if ( ev->m_eventType ==MT_Event::MT_INIT_REPONSE ){

    }

    if ( ev->m_eventType ==MT_Event::MT_CHGT_ETAT_PAU ){

    }

    if ( ev->m_eventType ==MT_Event::MT_SYNTHESE_ALARM_PAU ){

    }

}

void* SAPEX::SapexListeningThread(void* ctx)
{
    if ( ctx==NULL) return NULL;
    SAPEX* pSapex = (SAPEX*)ctx;

    while (!pSapex->_is_stopped )
    {
        usleep(ONE_SEC_USLEEP);

        pSapex->_sock = socket(AF_INET , SOCK_STREAM , 0);
        if (pSapex->_sock == -1){
            mylog(3,"Could not create socket pSapex->_sock");
            usleep(ONE_SEC_USLEEP * 10);continue;
        }

        mylog(3,"Socket pSapex->_sock created");
        if ( ::connect(pSapex->_sock , (struct sockaddr *)&pSapex->_sapex_server_sock , sizeof(pSapex->_sapex_server_sock)) < 0)
        {   //Connect to remote server
            mylog(3,"connect pSapex->_sock failed. Error");
            usleep(ONE_SEC_USLEEP * 10);continue;
        }else{
            mylog(3,"OK socket connected %d",pSapex->_sock);
        }

        mylog(5,"--> opcuaDataListeningThread on socket %d\n",pSapex->_sock);
        while(pSapex->_sock > 0 )//        while( 1 )
        {
            #define MAX_REPLY_HEADER_SIZE 3 // OPN MSG
            //int idx=0;
            int ret_recv=0;
            //unsigned char server_reply[MAX_REPLY_HEADER_SIZE+1]={0};
            unsigned char carcou1='.';
            unsigned char carcou2='.';
            unsigned char carcou3='.';
            //unsigned char chunkType='.';

            unsigned char buf[1*1024]={0};

            ret_recv = recv(pSapex->_sock , &carcou1 , 1 , 0);
            if ( ret_recv>0 ){
                //mylog(2,"A)rx %d: [%02X]\n",ret_recv, (unsigned char)carcou1);
                ret_recv = recv(pSapex->_sock , &carcou2 , 1 , 0);
                if ( ret_recv>0 ){
                    //mylog(2,"B)rx %d: [%02X]\n",ret_recv, (unsigned char)carcou2);
                    ret_recv = recv(pSapex->_sock , &carcou3 , 1 , 0);
                    if ( ret_recv>0 ){
                        //mylog(2,"C)rx %d: [%02X]\n",ret_recv, (unsigned char)carcou3);
                        QString reply;
                        reply+=carcou1;
                        //qDebug() << reply;
                        if ( reply == QString("#") )
                        {
                            mylog(2,"--------------from SAPEX recv [%c %02X %02X] string -------------------",carcou1,carcou2,carcou3);

                            WORD frameSize=0;
                            frameSize = (carcou2<<8) | carcou3;

                            mylog(2,"E)rx frameSize %d: [%d]\n",ret_recv, (unsigned char)frameSize);

                            uint idx=0;
                            while ( frameSize>0  && idx<frameSize && idx< (sizeof(buf)-1) )  {
                                ret_recv = recv(pSapex->_sock , &buf[idx] , 1 , 0);
                                if ( ret_recv>0 ){
                                    //mylog(2,"F)buf[%d]=[%02X]\n",idx, (unsigned char)buf[idx]);
                                    idx++;
                                }
                            }
                            //mylog(2,"F)buf[%d]=[%02X]\n",idx-1, (unsigned char)buf[idx-1]);
                            mylog(2,"------------------ RX from SAPEX FRAME (%d bytes) -----------------------",idx);

                            // todo : remplir les champs de l'event en fonction du json recu !
                            JSON_command jscmd(buf,frameSize);
                            SAPEX_Event::PRESCOM_EVENT_TYPE evt = jscmd.getEventType();

                            SAPEX_Event* pev = new SAPEX_Event;
                            if (pev){
                                pev->m_eventType = evt;
                                theBridge->_eventsReceivedFromSapex.push(pev);
                            }

                            QString m_json_outputFile = QApplication::applicationDirPath() + "/input.json";
                            QFile jsonFile(m_json_outputFile);
                            if(jsonFile.open(QIODevice::WriteOnly | QIODevice::Text))
                            {
                                QTextStream stream(&jsonFile);
                                stream << buf;
                                jsonFile.close();
                                qDebug() << "Writing finished";
                            }
                        }
                    }
                }
            }else if ( ret_recv==0 ){
                usleep(ONE_SEC_USLEEP);
            }
        }
    }
    mylog(5,"<-- SapexListeningThread ending normally\n");
    return NULL;
}

void SAPEX::readJsonFileTechRQ(QString openFileName)
{
    QFileInfo fileInfo(openFileName);   // Using QFileInfo
    QDir::setCurrent(fileInfo.path());  // set the current working directory where the file will be
    QFile jsonFile(openFileName);// Create a file object and open it for reading.
    if (jsonFile.open(QIODevice::ReadOnly|QIODevice::Text) )
    {
        QString json_string;
        json_string = jsonFile.readAll();
        jsonFile.close();

        qDebug() << json_string;
        QByteArray json_bytes = json_string.toLocal8Bit();
        auto json_doc = QJsonDocument::fromJson(json_bytes);

        if (json_doc.isNull()) {qDebug() << "Failed to create JSON doc." ;return;}else qDebug() << "OK create JSON doc.";
        if (!json_doc.isObject()) {qDebug() << "JSON is not an object." ;return;}qDebug() << "JSON is an object." ;

        QJsonObject json_obj = json_doc.object();
        if (json_obj.isEmpty()) {qDebug() << "JSON object is empty.";return;}else qDebug() << "JSON object is not empty.";

        qDebug() << "-----------------------------------";
        qDebug() << json_obj;
        qDebug() << "-----------------------------------";
        qDebug() << Q_FUNC_INFO << json_doc.toJson();

        qDebug() << "-----------------------------------";

        if(json_doc.isObject()) // objet racine ?
        {
            QJsonObject objetJSON = json_doc.object();
            QStringList listeCles;
            listeCles = objetJSON.keys();// Les clés
            qDebug() << "listeCles:" << listeCles; // une liste de QString
            qDebug() << listeCles.count() << " Cles au total" ;

            if ( listeCles.contains("type") ){

                QJsonValue vv = objetJSON.value("type");
                QString str_type  = vv.toString();
                qDebug() << "element type" << str_type;
                if ( str_type == "TECH_RQ"  )
                {
                    QJsonValue vv = objetJSON.value("header");
                    if (vv.isObject() )
                    {
                        QJsonObject ob_cat = vv.toObject();
                        qDebug() << "object json cat:" <<vv ;
                        QStringList kk;
                        kk = ob_cat.keys();// Les clés
                        qDebug() << "listeCles:" << kk; // une liste de QString
                        qDebug() << kk.count() << " Cles au total" ;
                        int nb = kk.count();
                        for(int i = 0; i <= (nb-1); i++){
                            qDebug() << "cle:" << i << ";" << kk[i] << "=" << ob_cat[kk[i]].toVariant().toString();
                        }
                    }
                    QJsonValue vvdata = objetJSON.value("data");
                    if (vvdata.isObject() ){
                        QJsonObject obj = vvdata.toObject();
                        qDebug() << "listeCles:" <<obj.keys();
                        int index = obj.keys().indexOf("val");//QRegExp("^bnd\|gf.+"));
                        qDebug() << index;
                        //if ( obj.keys().contains("val") )
                        if (index>=0 )
                        {
                            QJsonObject o3 = obj["val"].toObject();
                            qDebug() << "found o3:" <<  o3; //found val: QJsonValue(object, QJsonObject({"Demande":{"$":{"Seq":123456789},"PAU":"PAU0000056"}}))


                            qDebug() << "listeCles:" <<o3.keys();
                            int i4 = o3.keys().indexOf("Demande");
                            if (i4>=0 ){
                                QJsonObject o4 = o3["Demande"].toObject();
                                qDebug() << "found o4:" <<  o4;//found o4: QJsonObject({"$":{"Seq":123456789},"PAU":"PAU0000056"})
                                qDebug() << "listeCles:" << o4.keys();//listeCles: ("$", "PAU")
                                qDebug() << "PAU:" <<o4.value("PAU").toString();//PAU: QJsonValue(string, "PAU0000056")
                            }
                        }
                    }

                }
            }
            /*else for(int i = 0; i < listeCles.count()-1; i++){// Les valeurs
                QJsonValue valeur = objetJSON[listeCles.at(i)];
                //element 1 = QJsonValue(object, QJsonObject({"cat":"RAU","cat-type":"PAU","id":12889,"name":"SAPEX"}))
                qDebug() << "element" << i << "=" << valeur ; // un QJsonValue
            }*/
        }

        /*
        "{\"header\":{\"cat\":\"RAU\", \"cat-type\":\"PAU\", \"name\":\"SAPEX\", \"id\":12889}, \"data\":{\"timestamp\":\"2020-12-21 08:23:55.233\", \"val\":{\"Demande\": {\"$\": {\"Seq\":123456789}, \"PAU\": \"PAU0000056\"}}}, \"type\":\"TECH_RQ\"}"
        OK create JSON doc.
        JSON is an object.
        JSON object is not empty.
        -----------------------------------
        QJsonObject({"data":{"timestamp":"2020-12-21 08:23:55.233","val":{"Demande":{"$":{"Seq":123456789},"PAU":"PAU0000056"}}},"header":{"cat":"RAU","cat-type":"PAU","id":12889,"name":"SAPEX"},"type":"TECH_RQ"})
        -----------------------------------
        void MainWindow::on_pushButton_9_clicked() "{\n    \"data\": {\n        \"timestamp\": \"2020-12-21 08:23:55.233\",\n        \"val\": {\n            \"Demande\": {\n                \"$\": {\n                    \"Seq\": 123456789\n                },\n                \"PAU\": \"PAU0000056\"\n            }\n        }\n    },\n    \"header\": {\n        \"cat\": \"RAU\",\n        \"cat-type\": \"PAU\",\n        \"id\": 12889,\n        \"name\": \"SAPEX\"\n    },\n    \"type\": \"TECH_RQ\"\n}\n"
        -----------------------------------
        ("data", "header", "type")
        element 0 = QJsonValue(object, QJsonObject({"timestamp":"2020-12-21 08:23:55.233","val":{"Demande":{"$":{"Seq":123456789},"PAU":"PAU0000056"}}}))
        element 1 = QJsonValue(object, QJsonObject({"cat":"RAU","cat-type":"PAU","id":12889,"name":"SAPEX"}))
        */
    }

}

void SAPEX::writeJsonFileInit(QString filepath)
{
    QJsonObject dataObject;
    dataObject.insert("timestamp", QDateTime::currentDateTime().toString() );
    QJsonObject secObject;
    QJsonObject dollarObject;

    QJsonArray arralarmesPAUObject;
    QJsonObject alarmesPAUObject;
    alarmesPAUObject.insert("IdEqt", "PAU_00_0_00000");
    alarmesPAUObject.insert("DAC", "1");
    alarmesPAUObject.insert("DAL", "1");
    alarmesPAUObject.insert("SEC", "0");

    QJsonObject alarmesPAUObject2;
    alarmesPAUObject2.insert("IdEqt", "PAU_00_0_01001");
    alarmesPAUObject2.insert("DAC", "1");
    alarmesPAUObject2.insert("DAL", "1");
    alarmesPAUObject2.insert("SEC", "0");

    arralarmesPAUObject.insert(0,alarmesPAUObject);
    arralarmesPAUObject.insert(1,alarmesPAUObject2);


    secObject.insert("Seq", "1454546");
    QJsonObject etatsTechniquesObject;
    dollarObject.insert("$", secObject);

    dollarObject.insert("AlarmesPAU", arralarmesPAUObject);

    etatsTechniquesObject.insert("EtatsTechniques", dollarObject);
    dataObject.insert("val", etatsTechniquesObject);

    QJsonObject headerObject;
    headerObject.insert("cat", "RAU");
    headerObject.insert("cat-type", "PAU");
    headerObject.insert("name", "PRAUClient");
    headerObject.insert("id", "12548");

    QJsonDocument doc;
    QJsonObject objetJSON = doc.object();

    objetJSON.insert("type", QJsonValue::fromVariant("TECH"));// une clé/valeur de type chaine
    //objetJSON.insert("age", QJsonValue::fromVariant(50)); // une clé/valeur de type valeur
    objetJSON.insert("header", headerObject);
    objetJSON.insert("data", dataObject);

    doc.setObject(objetJSON);

    QString m_json_outputFile = filepath ;
    QFile jsonFile(m_json_outputFile);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(doc.toJson());
}


/*void traversJson(QJsonObject json_obj){
    foreach(const QString& key, json_obj.keys()) {
        QJsonValue value = json_obj.value(key);
        if(!value.isObject() ){
          qDebug() << "Key = " << key << ", Value = " << value;
         }
        else{
             qDebug() << "Nested Key = " << key;
             traversJson(value.toObject());
        }
    }
};*/

SAPEX_Event::PRESCOM_EVENT_TYPE JSON_command::getEventType()
{
    qDebug() << "JSON_command::getEventType:" << m_json_string;
    m_json_bytes = m_json_string.toLocal8Bit();
    auto json_doc = QJsonDocument::fromJson(m_json_bytes);
    if (json_doc.isNull()) {qDebug() << "Failed to create JSON doc." ;return SAPEX_Event::PRESCOM_INVALID;}else qDebug() << "OK create JSON doc.";
    if (!json_doc.isObject()) {qDebug() << "JSON is not an object." ;return SAPEX_Event::PRESCOM_INVALID;}qDebug() << "JSON is an object." ;
    QJsonObject json_obj = json_doc.object();
    if (json_obj.isEmpty()) {qDebug() << "JSON object is empty.";return SAPEX_Event::PRESCOM_INVALID;}else qDebug() << "JSON object is not empty.";

    if(json_doc.isObject()) // objet racine ?
    {
        QJsonObject objetJSON = json_doc.object();
        QStringList listeCles;
        listeCles = objetJSON.keys();
        qDebug() << listeCles; // une liste de QString ex: trame INIT: ("data", "header", "type")

        if ( listeCles.contains("type") ){

            QJsonValue vv = objetJSON.value("type");
            QString str_type  = vv.toString();
            qDebug() << "element type" << str_type;
            if ( str_type == "TECH_RQ"  ) return SAPEX_Event::PRESCOM_TECH_RQ;
            if ( str_type == "SYNT_RQ"  ) return SAPEX_Event::PRESCOM_SYNT_RQ;
            if ( str_type == "INIT"  ) return SAPEX_Event::PRESCOM_INIT;
            if ( str_type == "TECH_RQ"  ) return SAPEX_Event::PRESCOM_INVALID;
        }
#if 0
        for(int i = 0; i < listeCles.count()-1; i++){// Les valeurs
            QJsonValue valeur = objetJSON[listeCles.at(i)];
            //element 1 = QJsonValue(object, QJsonObject({"cat":"RAU","cat-type":"PAU","id":12889,"name":"SAPEX"}))
            qDebug() << "element" << i << "=" << valeur ; // un QJsonValue
            /*element 0 = QJsonValue(object, QJsonObject({"timestamp":"2020-12-21 08:23:55.233","val":{"opc_sec_mode":"Sign & encrypt","opc_sec_policy":"Basic256sha256","opc_server_name":"MTRAUServer","opc_server_port":8383,"pau_list":[{"PAU0000056":"PAU_01_0_00056"},{"PAU0000053":"PAU_01_0_00053"},{"PAU0000202":"PAU_02_1_00202"},{"PAU0000000":"PAU_00_0_00000"}]}}))*/
            /* element 1 = QJsonValue(object, QJsonObject({"cat":"RAU","cat-type":"PAU","id":12889,"name":"SAPEX"}))*/

        }
#endif
    }
    return SAPEX_Event::PRESCOM_INVALID;
}



void SAPEX::readJsonFileInit(QString openFileName)
{

    SAPEX_Event* pev = new SAPEX_Event;

    QFileInfo fileInfo(openFileName);   // Using QFileInfo
    QDir::setCurrent(fileInfo.path());  // set the current working directory where the file will be
    QFile jsonFile(openFileName);
    if (jsonFile.open(QIODevice::ReadOnly|QIODevice::Text) )
    {
            QString json_string;
            json_string = jsonFile.readAll();
            qDebug() << json_string;
            jsonFile.close();


            unsigned char buf[JSON_MAX_SIZE]={0};
            uint buf_len = json_string.length();
            for ( uint i=0; i<buf_len; i++){
                buf[i] = (unsigned char)json_string.at(i).toLatin1();
            }

            JSON_command jj;
            //jj.m_json_string = json_string;
            jj.fromBuffer(buf,buf_len);
            SAPEX_Event::PRESCOM_EVENT_TYPE ev = jj.getEventType();// = SAPEX_Event::PRESCOM_INIT;
            pev->m_eventType = ev;
            jj.extract_PAU_from_INIT();
            pev->m_list_PAU = jj.m_list_PAU;
            pev->MT_opcua_server_url = __MT_OPCUA_URL__; // force address of OPCUA Server MT
            theBridge->_eventsReceivedFromSapex.push(pev);

            return;
#if 0
            QByteArray json_bytes = json_string.toLocal8Bit();
            auto json_doc = QJsonDocument::fromJson(json_bytes);

            if (json_doc.isNull()) {qDebug() << "Failed to create JSON doc." ;return;}qDebug() << "OK create JSON doc.";
            if (!json_doc.isObject()) {qDebug() << "JSON is not an object." ;return;}else qDebug() << "JSON is an object." ;

            QJsonObject json_obj = json_doc.object();
            if (json_obj.isEmpty()) {qDebug() << "JSON object is empty.";return;}else qDebug() << "JSON object is not empty.";

            qDebug() << "-----------------------------------";
            qDebug() << json_obj;
            qDebug() << "-----------------------------------";
            qDebug() << Q_FUNC_INFO << json_doc.toJson();

            qDebug() << "-----------------------------------";

            if(json_doc.isObject()) // objet racine ?
            {
                QJsonObject objetJSON = json_doc.object();
                QStringList listeCles;
                listeCles = objetJSON.keys();

                // Les clés
                qDebug() << listeCles; // une liste de QString

                // Les valeurs
                for(int i = 0; i < listeCles.count()-1; i++)
                {
                    QJsonValue valeur = objetJSON[listeCles.at(i)];

                    //element 1 = QJsonValue(object, QJsonObject({"cat":"RAU","cat-type":"PAU","id":12889,"name":"SAPEX"}))
                    qDebug() << "element" << i << "=" << valeur ; // un QJsonValue

                    if ( valeur.isObject() ){
                        QJsonObject oo = valeur.toObject();
                        QJsonValue valeur = oo.value(QString("cat"));
                        //Les valeurs de type QJsonValue peuvent être converties dans leur type :

                        // 6 types
                        if(valeur.isArray())
                        {
                           qDebug() << valeur.toArray(); // le type QJsonArray
                           QJsonArray jsarr = valeur.toArray();
                           qDebug() << jsarr[0];  // Pour obtenir la première valeur du tableau
                        }
                        else if(valeur.isBool())
                        {
                           qDebug() << valeur.toBool();
                        }
                        else if(valeur.isDouble())
                        {
                           qDebug() << valeur.toDouble();
                        }
                        else if(valeur.isObject())
                        {
                           qDebug() << valeur.toObject(); // le type QJsonObject
                        }
                        else if(valeur.isString())
                        {
                           qDebug() << valeur.toString();
                        }

                        qDebug() << "OBJECT:" << oo ;
                        qDebug() << "cat:" << oo.value("cat").toString();
                        qDebug() << "cat-type:" << oo.value("cat-type").toString();
                        qDebug() << "id:" << oo.value("id").toInt();
                        qDebug() << "name:" << oo.value("name").toString();

                    }

                    // 6 types
                    if(valeur.isArray())
                    {
                       qDebug() << QString("[tableau]");
                    }
                    else if(valeur.isBool())
                    {
                       qDebug() << QString::fromUtf8("[booléen]");
                    }
                    else if(valeur.isDouble())
                    {
                       qDebug() << QString("[valeur]");
                    }
                    else if(valeur.isNull())
                    {
                       qDebug() << QString("[null]");
                    }
                    else if(valeur.isObject())
                    {
                       qDebug() << QString("[objet]");
                    }
                    else if(valeur.isString())
                    {
                       qDebug() << QString::fromUtf8("[chaîne]");
                    }
                }
            }
#endif
    }
}


