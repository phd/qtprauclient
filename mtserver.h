#ifndef MTSERVER_H
#define MTSERVER_H

#include <QtOpcUa/QOpcUaClient>
#include <QtOpcUa/QOpcUaProvider>

#include "utils.h"

class myOpcuaNode :public QObject
{
    Q_OBJECT
public:
    bool m_bowsing;
    myOpcuaNode(){}
    ~myOpcuaNode(){}
    QVariant m_variant;
    QString m_nodename ;
    QString m_nodeId ;
    QOpcUaNode* m_internalNode;
    QMap<QString ,myOpcuaNode*> m_subnodes;
    void readValueAttribute();
    void monitorAttribute();
    void browseChildren();
    void writeValueAttribute(QString nodeValue); //QOpcUa::nodeIdFromInteger(6,1410065343) , value )
    myOpcuaNode* findSubNodeByNodeId(QString nodeid);
public slots:
    //void slot_browse_read_attribute(QOpcUa::NodeAttributes attr);
    void slot_attribute_read(QOpcUa::NodeAttributes attr);
    void slot_monitoredNodeDataChangeOccurred(QOpcUa::NodeAttribute attr, const QVariant &value);
    void slot_browseChildrenFinished(QVector<QOpcUaReferenceDescription> children, QOpcUa::UaStatusCode statusCode);

};

class myOpcuaServer :public QObject
{
    Q_OBJECT
public:
    void init(QString url);
    QOpcUaProvider *mOpcUaProvider;
    QOpcUaClient *mOpcUaClient;    
    void disconnect();
    void NodeWriteValueAttribute(int ns, int id_node, QString contenu) ; //QOpcUa::nodeIdFromInteger(6,1410065343) , value )
    void NodeReadValueAttribute(int ns, int id_node, QString& nodeValue);
    //void NodeMonitorAttribute(int ns, int id_node, QString& nodeValue);
    QOpcUaNode* m_rootNode;
    QOpcUaNode* m_current_monitored_node;
    myOpcuaNode* findNodeByName(QString name);
    myOpcuaNode* findNodeByNodeId(QString nodeid);
    QStringList m_list_PAU;
    QStringList m_tag_list;
    void browseObjectNodeChildren();
    void browseObjectNodeChildrenSubnodes()    ;
    bool m_ClientConnected;
    void sendINITresponse();
    void enableMonitoringTag()    ;
private slots:
    void slot_browseFinished(QVector<QOpcUaReferenceDescription> children, QOpcUa::UaStatusCode statusCode);
    void slot_clientConnected();
    void slot_clientDisconnected();
    //void slot_attribute_read(QOpcUa::NodeAttributes attr);
    //void slot30(QOpcUa::NodeAttributes attr);
    //void slot6(QOpcUa::NodeAttribute attr, const QVariant &value);
    //void slot_monitoredNodeDataChangeOccurred(QOpcUa::NodeAttribute attr, const QVariant &value);

};


#endif // MTSERVER_H
