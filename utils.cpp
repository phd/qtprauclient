#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QMutex>
#include <QtCore>
#include <QString>
#include <QThread>
#include <QQueue>
#include <QMutex>
#include <QMap>
#include <QApplication>
#include <QCoreApplication>
#include <QTimer>
#include <QTextStream>
#include <QStringList>
#include <QLinkedList>
#include <QEvent>

static int log_level = 4;

void mylog(unsigned char level,const char* fmt, ...)
{
   //return; //phd20161010
   if ( level > log_level ) return;
    static QMutex mut;
    if (mut.tryLock()){
        if ( 0==fmt ) return;
        if ( 0==fmt[0] ) return;
        if ( '\n'==fmt[0] ) return;

        struct timeval tv;
        gettimeofday (&tv, NULL);
        long milliseconds;
        milliseconds = tv.tv_usec / 1000;

        char time_string[40];
        struct tm* ptm = localtime (&tv.tv_sec);
        strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);

        va_list args;
        va_start(args, fmt);
        //nb = vsnprintf(temp, sizeof(temp) , fmt, args);
        char temp[1024]={0};
        vsprintf(temp, fmt, args);
        va_end (args);

        // oter tous les '\n' en fin de chaine
        while ( temp!=NULL && temp[0] && temp[strlen(temp)-1] == '\n' ) temp[strlen(temp)-1] = 0;

        char horodate[128];
        time_t t = time(NULL);
        strftime(horodate, sizeof(horodate), "%Y-%m-%d", localtime(&t));
        char filename[128];
        //sprintf(filename,"/var/log/recorder/log-%s-PID-%d-%s.log",progname,getpid(),horodate);
        sprintf(filename,"./log/log-opcua-PID-%d-%s.log",getpid(),horodate);

        FILE* fout=fopen(filename,"a+t") ;
        if ( fout) {
            //strftime(horodate, sizeof(horodate), "%Y-%m-%d %H:%M:%S", localtime(&t));
            strftime(horodate, sizeof(horodate), "%H:%M:%S", localtime(&t));
            fprintf(fout,"%s.%03ld %s\n",horodate,milliseconds,temp);
            fclose(fout);
            fprintf(stderr,"%s.%03ld %s\n",horodate,milliseconds,temp);
        }else{
            fprintf(stderr,"log file error [%s]\n",filename);
        }
        mut.unlock();
    }
}
