#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>

#include <QMessageBox>
#include <QSettings>
#include <QMutex>

#include "mtserver.h"
#include "globales.h"

void myOpcuaNode::slot_attribute_read(QOpcUa::NodeAttributes attr)
{
    qDebug() << "myOpcuaNode::slot_attribute_read:" << m_internalNode->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();
    /*myOpcuaNode::slot_attribute_read: "ALERTE_APPEL"
    myOpcuaNode::slot_attribute_read: "AUDIO_REC"
    myOpcuaNode::slot_attribute_read: "CDE_ORD_PAU"*/
    m_nodename = m_internalNode->attribute(QOpcUa::NodeAttribute::BrowseName).value<QOpcUa::QQualifiedName>().name();
    m_variant =  m_internalNode->valueAttribute() ;//ptr_node->writeValueAttribute("44");
    qDebug() << "variant:"<<m_variant;
}

void myOpcuaNode::readValueAttribute()
{
    connect(m_internalNode, &QOpcUaNode::attributeRead, this, &myOpcuaNode::slot_attribute_read);
    if (!m_internalNode->readAttributes( QOpcUa::NodeAttribute::Value
                            | QOpcUa::NodeAttribute::NodeClass
                            | QOpcUa::NodeAttribute::Description
                            | QOpcUa::NodeAttribute::DataType
                            | QOpcUa::NodeAttribute::BrowseName
                            | QOpcUa::NodeAttribute::DisplayName
                            ))
        qWarning() << "myOpcuaNode::readValueAttribute" << m_internalNode->nodeId() << "failed";
}

void myOpcuaNode::writeValueAttribute(QString nodeValue) //QOpcUa::nodeIdFromInteger(6,1410065343) , value )
{
    QVariant value(nodeValue);
    qDebug() << value << " with nodeValue: " << nodeValue;

    QString nodeType = "unsigned char";//QString nodeType = "double";
    const auto type = QVariant::nameToType(nodeType.toLatin1().constData());
    if(type == QVariant::Type::Invalid) {qCritical() << "Invalid node type";return;}

    if(!value.convert(type)) {
        qCritical() << "Value cannot be presented as given type " << nodeType;
        return;
    }
    //QOpcUaNode* m_CDE_ORD_PAU = mOpcUaClient->node(QOpcUa::nodeIdFromInteger(ns,id_node));
    //if (m_CDE_ORD_PAU)m_CDE_ORD_PAU->writeValueAttribute(value);
    m_internalNode->writeValueAttribute(value);

}


myOpcuaNode* myOpcuaNode::findSubNodeByNodeId(QString nodeid)
{
    QMap<QString, myOpcuaNode*>::const_iterator i = m_subnodes.find(nodeid);
    if (i != m_subnodes.end()) {
        myOpcuaNode* ptr_node = i.value();
        //current_node = ptr_node;
        //cout << i.value() ;
        //m_plist2->insertItem(0, QString("children current node %1 %2").arg(ptr_node->m_nodeId ).arg(ptr_node->m_nodename) );
        return ptr_node; //ptr_node->browseChildren();
    }
    return NULL;
}

void myOpcuaNode::browseChildren()
{
    m_bowsing = true;
    connect(m_internalNode, &QOpcUaNode::browseFinished, this, &myOpcuaNode::slot_browseChildrenFinished);
    if (!m_internalNode->browseChildren())
        qDebug() << "Browsing node" << m_internalNode->nodeId() << "failed";
    else
        qDebug() << "Browsing node" << m_internalNode->nodeId() << "OK";
}

void myOpcuaNode::slot_browseChildrenFinished(QVector<QOpcUaReferenceDescription> children, QOpcUa::UaStatusCode statusCode)
{
    //QMessageBox msgBox;msgBox.setText("myOpcuaServer::browseFinished");msgBox.exec();
    if (statusCode != QOpcUa::Good) {
        qWarning() << "Browsing node" << m_internalNode->nodeId() << "finally failed:" << statusCode;
        return;
    }else    qWarning() << "Browsing node" << m_internalNode->nodeId() << "finally OK:" << statusCode;
    for (const auto &item : children) {//qDebug() << "sub node:" << item.targetNodeId();

        myOpcuaNode* ptr = NULL;// findSubNodeByNodeId(item.targetNodeId().nodeId());
        if ( ptr==NULL){
            qDebug() << "sub node nodeId:" << item.targetNodeId().nodeId() << " serverIndex:" << item.targetNodeId().serverIndex() << " namespaceUri:" << item.targetNodeId().namespaceUri();
            QOpcUaNode* sub_QOpcUaNode = theOpcuaServer->mOpcUaClient->node(item.targetNodeId());
            if (!sub_QOpcUaNode) {
                qWarning() << "Failed to instantiate node:" << item.targetNodeId().nodeId();
            }else{
                // todo : proteger contre 2 insertion du meme node
                ptr=new myOpcuaNode;
                ptr->m_nodeId = item.targetNodeId().nodeId();
                ptr->m_internalNode = sub_QOpcUaNode;
                m_subnodes.insert(item.targetNodeId().nodeId(), ptr );
                qDebug() << "--> readValueAttribute m_subnodes.insert child node:" << sub_QOpcUaNode->nodeId();
                ptr->readValueAttribute(); // --> retrieve ptr->m_nodename
            }
        }else{
            qDebug() << "ALREADY HERE sub node nodeId:" << item.targetNodeId().nodeId();
        }
    }
    m_bowsing = false;
}
/*--> readValueAttribute m_subnodes.insert child node: "ns=6;i=1410065360"
myOpcuaNode::slot_attribute_read: "AAL"
myOpcuaNode::slot_attribute_read: "AT1"
myOpcuaNode::slot_attribute_read: "AT2"
myOpcuaNode::slot_attribute_read: "CO1"
myOpcuaNode::slot_attribute_read: "CO2"
myOpcuaNode::slot_attribute_read: "DAC"
myOpcuaNode::slot_attribute_read: "DAL"
myOpcuaNode::slot_attribute_read: "DTR"
myOpcuaNode::slot_attribute_read: "EA1"
myOpcuaNode::slot_attribute_read: "ESA"
myOpcuaNode::slot_attribute_read: "IPS"
myOpcuaNode::slot_attribute_read: "ISS"
myOpcuaNode::slot_attribute_read: "NOF"
myOpcuaNode::slot_attribute_read: "OCC"
myOpcuaNode::slot_attribute_read: "OP1"
myOpcuaNode::slot_attribute_read: "OP2"
myOpcuaNode::slot_attribute_read: "PHB"
myOpcuaNode::slot_attribute_read: "PHO"
myOpcuaNode::slot_attribute_read: "RAD"
myOpcuaNode::slot_attribute_read: "RPF"
myOpcuaNode::slot_attribute_read: "RSA"
myOpcuaNode::slot_attribute_read: "RSF"
myOpcuaNode::slot_attribute_read: "SEC"
myOpcuaNode::slot_attribute_read: "SEQ"
myOpcuaNode::slot_attribute_read: "TON"
myOpcuaNode::slot_attribute_read: "TRA"
myOpcuaNode::slot_attribute_read: "ALERTE_APPEL"
myOpcuaNode::slot_attribute_read: "AUDIO_REC"
myOpcuaNode::slot_attribute_read: "CDE_ORD_PAU"
myOpcuaNode::slot_attribute_read: "CDE_REG_OPE"
myOpcuaNode::slot_attribute_read: "ETAT_PAU"
myOpcuaNode::slot_attribute_read: "OP_EN_COM"
myOpcuaNode::slot_attribute_read: "POS_ANT_PIR"
myOpcuaNode::slot_attribute_read: "POS_ANT_SECU"
myOpcuaNode::slot_attribute_read: "POS_AXE"
myOpcuaNode::slot_attribute_read: "POS_HORODATE_GPS"
myOpcuaNode::slot_attribute_read: "POS_LAT"
myOpcuaNode::slot_attribute_read: "POS_LOC_PIR"
myOpcuaNode::slot_attribute_read: "POS_LONG"
myOpcuaNode::slot_attribute_read: "POS_MOBILE"
myOpcuaNode::slot_attribute_read: "POS_NUM_TEL"
myOpcuaNode::slot_attribute_read: "POS_PRMETRE"
myOpcuaNode::slot_attribute_read: "POS_REPERE"
myOpcuaNode::slot_attribute_read: "POS_SENS"
myOpcuaNode::slot_attribute_read: "STA_PAU"
myOpcuaNode::slot_attribute_read: "TC_PILOTER_PAU"*/

void myOpcuaNode::slot_monitoredNodeDataChangeOccurred(QOpcUa::NodeAttribute attr, const QVariant &value)
{
    Q_UNUSED(attr);
    QString str=  QString("QVariant integer:%1").arg(value.toUInt());
    QMessageBox msgBox;msgBox.setText(QString("QOpcUa::NodeAttribute::Value=")+str);msgBox.exec();
    //QMessageBox::information(this, tr("enableMonitoring"), QString("QOpcUa::NodeAttribute::Value=")+str);//
}

void myOpcuaNode::monitorAttribute()
{
    connect(m_internalNode, &QOpcUaNode::dataChangeOccurred, this, &myOpcuaNode::slot_monitoredNodeDataChangeOccurred);
    m_internalNode->enableMonitoring(QOpcUa::NodeAttribute::Value,QOpcUaMonitoringParameters(300));
}

