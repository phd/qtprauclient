#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>
#include <QFile>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QMutex>

/*
PAU_node::PAU_node(){
    m_CDE_ORD_PAU=NULL;
    m_ETAT_PAU = NULL;
    m_nodename = "";
}

PAU_node::~PAU_node(){}

void PAU_node::slot_dataChangeOccurred(QOpcUa::NodeAttribute attr, const QVariant &value)
{
    QString str=  QString("QVariant %2 integer:%1").arg(m_nodename).arg(value.toUInt());
    qDebug() << str;
    QMessageBox msgBox;msgBox.setText(str+"<-slot1");msgBox.exec();
    //QMessageBox::information(this, tr("enableMonitoring"), QString("QOpcUa::NodeAttribute::Value=%1 %2").arg(m_nodename).arg(str) );//
}

bool PAU_node::monitor_FONC(QOpcUaClient *cli)
{
    if ( m_CDE_ORD_PAU ==NULL) m_CDE_ORD_PAU = cli->node(QOpcUa::nodeIdFromInteger(6,1410065343));
    if (m_CDE_ORD_PAU)
    {
        QMessageBox msgBox;msgBox.setText("monitor_FONC");msgBox.exec();
        //QOpcUaMonitoringParameters p(1000, QOpcUaMonitoringParameters::SubscriptionType::Exclusive);
        connect(m_CDE_ORD_PAU, &QOpcUaNode::dataChangeOccurred, this, &PAU_node::slot_dataChangeOccurred);
        m_CDE_ORD_PAU->enableMonitoring(QOpcUa::NodeAttribute::Value,QOpcUaMonitoringParameters(300));
        return true;
    }else return false;
}

bool PAU_node::set_FONC(QOpcUaClient *cli,QString& val)
{
    if ( m_CDE_ORD_PAU ==NULL) m_CDE_ORD_PAU = cli->node(QOpcUa::nodeIdFromInteger(6,1410065343));
    if (m_CDE_ORD_PAU)
    {
        QVariant value(val);
        qDebug() << " set_FONC : " << val;
        QString nodeType = "unsigned char";//QString nodeType = "double";
        const auto type = QVariant::nameToType(nodeType.toLatin1().constData());
        if(type == QVariant::Type::Invalid) {qCritical() << "Invalid node type";return false;}
        if(!value.convert(type)) {
            qCritical() << "Value cannot be presented as given type " << nodeType;
            return false;
        }else
            m_CDE_ORD_PAU->writeValueAttribute(value);
        return true;
    }else return false;
}
*/

