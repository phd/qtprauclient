#ifndef GLOBALES_H
#define GLOBALES_H

#include "utils.h"
#include "mtserver.h"
#include "sapex.h"

//extern SAPEX* m_sapex;
extern QOpcUaProvider *mOpcUaProvider;
extern QOpcUaClient *mOpcUaClient;
extern QMap<QString ,myOpcuaNode*>     g_object_node_children_map;
//extern QSet<QString> g_object_node_children_id;

extern SAPEX* theSapex;
extern myOpcuaServer* theOpcuaServer;
extern Bridge* theBridge;

extern QString MT_opcua_server_url;
#define __MT_OPCUA_URL__                "opc.tcp://192.168.8.183:8383"
#define __NODEID_OPCUA_OBJECTS_ROOT__   "ns=0;i=85" /* nom du node object racine sous lequel se trouvent les PAU */

#endif // GLOBALES_H
