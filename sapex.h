#ifndef SAPEX_H
#define SAPEX_H

#define JSON_MAX_SIZE 1024

#include <net/ethernet.h>
#include <netinet/ip_icmp.h>   //Provides declarations for icmp header
#include <netinet/udp.h> //Provides declarations for udp header
#include <netinet/tcp.h> //Provides declarations for tcp header
#include <netinet/ip.h>  //Provides declarations for ip header
#include <arpa/inet.h> //inet_addr


#include <QtOpcUa/QOpcUaClient>
#include <QtOpcUa/QOpcUaProvider>

#include "utils.h"



class MT_Event: public QObject
{
    Q_OBJECT
public:
    typedef enum{
        MT_INIT_REPONSE= 1,
        MT_CHGT_ETAT_PAU,
        MT_SYNTHESE_ALARM_PAU,
    }MT_EVENT_TYPE;
    MT_EVENT_TYPE m_eventType;
    MT_Event(){}
    unsigned char m_buf[JSON_MAX_SIZE];
    uint m_frameSize;
    QString m_PAU_nodename;
    QStringList  m_PAU_Tag_list;
    QMap<QString ,QMap <QString,QString> >  m_PAU_TagnameValue_map;
};

class SAPEX_Event: public QObject
{
    Q_OBJECT
public:
    typedef enum{
        PRESCOM_INVALID = 0,
        PRESCOM_INIT = 1,
        PRESCOM_SYNT_RQ,
        PRESCOM_TECH_RQ,
    }PRESCOM_EVENT_TYPE;
    PRESCOM_EVENT_TYPE m_eventType;
    QString MT_opcua_server_url;
    QStringList m_list_PAU;

    SAPEX_Event(){}
    SAPEX_Event(unsigned char* buf,uint frameSize);
    unsigned char m_buf[JSON_MAX_SIZE];
    uint m_frameSize;
};

class JSON_command: public QObject
{
    Q_OBJECT
public:
    JSON_command(){}
    JSON_command(unsigned char* buf,uint frameSize);
    unsigned char m_buf[JSON_MAX_SIZE];
    uint m_frameSize;
    QString m_json_string;
    QStringList m_list_PAU;
    QByteArray m_json_bytes;
    SAPEX_Event::PRESCOM_EVENT_TYPE getEventType();
    void extract_PAU_from_INIT();
    void fromBuffer(unsigned char* buf,uint frameSize);
};

class Bridge : public QThread
{
public:
    QAsyncQueue<MT_Event*>  _eventsReceivedFromMT;
    QAsyncQueue<SAPEX_Event*>  _eventsReceivedFromSapex;
    void    run();
};

class SAPEX :public QObject
{
    Q_OBJECT
public:
    void init(QString , uint);
    SAPEX(){/*_commandsToSend.setMax(100);_commandsReceived.setMax(100);*/}
    //QAsyncQueue<JSON_command>  _commandsToSend;
    //QAsyncQueue<JSON_command*>  _commandsReceived;

    QString _sapex_ip_addr;
    uint _sapex_port;
    bool                        _is_stopped;
    pthread_t                   _thread_id;
    bool                        _isConnected;
    int                         _sock;
    struct sockaddr_in          _sapex_server_sock;

    static void* SapexListeningThread(void* ctx);

    void readJsonFileTechRQ(QString filepath);
    void readJsonFileInit(QString filepath);
    void writeJsonFileInit(QString filepath);

    void sendJSONtoSAPEX(MT_Event* ev)   ;
};

#endif // SAPEX_H
